import React  from "react";
import Routes from './Routes.js';
import firebase from 'firebase';
import {firebaseConfig} from './src/config';
firebase.initializeApp(firebaseConfig);


const App = () => <Routes/>

export default App;
