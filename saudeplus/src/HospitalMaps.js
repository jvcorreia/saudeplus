import React, { Component } from 'react';
import { View, StyleSheet,TouchableOpacity,Text, ScrollView } from 'react-native';
import { WebView } from 'react-native-webview';


class Page10 extends Component {
  static navigationOptions = {
    header: null,
  };
  render() {
    return (
      <View style={styles.container}>
      <View style={styles.container2}>
      <WebView
        source={{uri: 'https://www.google.com.br/maps/search/hospital/'}}
        style={{marginTop: 0}}
      />
      </View>
      
      <TouchableOpacity style={styles.button2} onPress={() => this.props.navigation.navigate('Menu')} >
          <Text style={styles.textButton2} 
          > Voltar ao menu </Text>
        </TouchableOpacity>
        </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ACCEE8',
    flex:1
  },
 container2: {
    backgroundColor: '#ACCEE8',
    flex: 1
    
  },

      image2: { marginTop:-205,
    left:5,
    width:  200, 
    height: 210,
      },



     button2: {
    flex:0.08,
     alignItems:'center',
      justifyContent:'center',

    
    
    backgroundColor: '#FF7A7D',
    top:0,
    borderColor: '#ff6505', 
    borderRadius:25,
    

  },
    textButton2: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: '800',
    fontSize: 22,
    marginTop:-10
   
  },


  
});
export default Page10;