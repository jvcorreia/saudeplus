import React, { Component } from 'react';
import { Text, View, Image, TextInput, TouchableOpacity, StyleSheet
 ,Alert, Modal, ScrollView, ImageBackground} from 'react-native';



 class Page8 extends Component {
   static navigationOptions = {
    header: null,
  };


  render() {
    const { navigate } = this.props.navigation;
    return (
            <View style={styles.container}> 


              <ImageBackground 
                source={require('./images/back.jpg')}
                style={{flex:1,alignItems:'center',}} 
              > 


                <View style={styles.image}> 
                  <Image style={styles.image} source={require('./images/historia.png')}/> 
                </View>     
               
                  <Text style={styles.text}>Criador:</Text> 
                  <Text style={styles.text2}>João Victor Correia de Oliveira</Text>
                  <Text style={styles.text}>Agradecimentos:</Text>
                  <Text style={styles.text3}>Professor Caio;{"\n"}amigos;{"\n"}e família.</Text>
                  <Text style={styles.text}>E-mail:</Text>
                  <Text style={styles.text3}>jvcorreia@sempreceub.com</Text>
                  <Text style={styles.text}>LinkedIn</Text>
                  <Text style={styles.text3}onPress={() => this.props.navigation.navigate('LinkedIn')}>Clique aqui</Text>

                  <TouchableOpacity style={styles.button}>
                  <Text style={styles.textButton} onPress={() => this.props.navigation.navigate('Menu')}> Voltar  </Text>
                  </TouchableOpacity>

              </ImageBackground>  


              </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ACCEE8',
    flex: 1
  },
 containermodal: {
    backgroundColor: '#FFDBD9',
    justifyContent:'center',
    alignItems:'center'
  },
  image: { 
    width:  250, 
    height: 250
  },
  button: {
    height: 45,
    width: 160,
    padding: 15,
    backgroundColor: '#FF7A7D',
    margin:10,
    borderColor: '#ff6505', 
    borderRadius:25,
    borderColor:'#fff',
    borderWidth:2
  },
  textButton: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 19,
    marginTop:-7
  },
  text: {
    padding:10,
    fontSize: 20,
    marginTop:-20, 
    top:-30, 
    alignItems:'center',
    justifyContent:'center',
    color: '#000',
    textAlign: 'center',
    fontWeight: '900',
    fontSize:22
  },
  text2: {
    margin:10,
    padding:10,
    fontSize: 20,
    top:-36,
    marginTop:-20,  
    alignItems:'center',
    justifyContent:'center',
    color: '#000',
    textAlign: 'center',
    fontWeight: '300',
    fontSize: 19,
    textDecorationLine:'underline',
  },
  text3: {
    margin:10,
    padding:10,
    fontSize: 20,
    top:-34,
    marginTop:-20,  
    alignItems:'center',
    justifyContent:'center',
    color: '#000',
    textAlign: 'center',
    fontWeight: '300',
    fontSize: 19,
    textDecorationLine:'underline',
  }

});

 export default Page8;