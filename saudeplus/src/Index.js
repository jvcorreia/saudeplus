import React, { Component } from 'react';
import { Text, View, Image, TextInput, TouchableOpacity, StyleSheet ,Alert,
 ScrollView, ImageBackground} from 'react-native';
import firebase from 'react-native-firebase';


 class Page1 extends Component {
   static navigationOptions = {
    header: null,
  };


  render() {
    const { navigate } = this.props.navigation;
    return (
  
      <View style={styles.container}>
        
        <ImageBackground 
          source={require('./images/back.jpg')}
          style={{flex:1,alignItems:'center',}} 
        >
          <Image style={styles.image} source={require('./images/indez.png')}/>
             
          <Text style={styles.textIndex}>Faça seu diagnóstico;{"\n"} Busque por farmácias próximas; {"\n"} Recomendações para o seu problema;{"\n"}Tudo aqui.</Text>         
              
          <TouchableOpacity style={styles.button} onPress={() => this.props.navigation.navigate('Login')} >
            <Text style={styles.textButton}> Entrar </Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.button2} onPress={() => this.props.navigation.navigate('Cadastro')} >
            <Text style={styles.textButton2}> Criar conta gratuita </Text>
          </TouchableOpacity>
        
        </ImageBackground>
               
      </View>
       
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ACCEE8',
    flex:1,
    justifyContent:'center'
  },
  image: { margin:50,
    width:  220, 
    height: 220,
    alignItems:'center'
  },
  button: {
    height: 70,
    width: 325,
    padding: 15,
    backgroundColor: '#FF7A7D',
    margin:10,
    borderRadius:25,
    alignItems:'center'
  },
  button2: {
    height: 70,
    width: 325,
    alignItems:'center',
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderRadius:25
  },
  textButton: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 30,
    marginTop:-5
  },
  textIndex: {
    color: '#000',
    textAlign: 'center',
    fontWeight: '900',
    fontSize: 19,
    marginTop:-5
  },
  textButton2: {
    color: '#FF7A7D',
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 30,
    marginTop:-5
  }
});

 export default Page1;