import React, { Component } from 'react';
import { Text, View, Image, TextInput, TouchableOpacity, StyleSheet ,Alert, Modal,
 ScrollView, ImageBackground} from 'react-native';
import firebase from 'firebase';
import {
  LoginButton,
  AccessToken
} from 'react-native-fbsdk';
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';


 class Page11 extends Component {
   static navigationOptions = {
    header: null,
  };
 

state = { email: '', password: '',visible1:false,visible2:false, userInfo:''}

handleLogin = async() => {

        const { email, password,visible1,visible2 } = this.state;

        if( (email == '') || (password == '') || (email == '' && password == '') ){
            this.setState({ visible1: true });      
        }

        else{
            try {
                const user = await firebase.auth().signInWithEmailAndPassword(email, password);
                this.props.navigation.navigate('Menu');  
            }
            catch(err){
                this.setState({ visible2: true });  
            }
        }
    }


  handleCancel1 = () => {
    this.setState({ visible1: false });
  }
  handleCancel2 = () => {
    this.setState({ visible2: false });
  }


  componentDidMount() {
    GoogleSignin.configure({
      //It is mandatory to call this method before attempting to call signIn()
      scopes: ['https://www.googleapis.com/auth/drive.readonly'],
      // Repleace with your webClientId generated from Firebase console
      webClientId:
        '1075821909757-d6040j1a16ggibm3q9a53k1f93ihdgsp.apps.googleusercontent.com',
    });
  }

  async signIn() {
    await this.configPromise;
    return await RNGoogleSignin.signIn();
  }

_signIn = async () => {
    //Prompts a modal to let the user sign in into your application.
    try {
      await GoogleSignin.hasPlayServices({
        //Check if device has Google Play Services installed.
        //Always resolves to true on iOS.
        showPlayServicesUpdateDialog: true,

      });
      const userInfo = await GoogleSignin.signIn();
      console.log('User Info --> ', userInfo);
      this.setState({ userInfo: userInfo });
      alert('Logado com sucesso');
      this.props.navigation.navigate('Menu');
    } catch (error) {
      console.log('Message', error.message);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('User Cancelled the Login Flow');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('Signing In');
        
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('Play Services Not Available or Outdated');
      } else {
        console.log('Some Other Error Happened');
        this.props.navigation.navigate('Menu');

      }
    }
  };

  isSignedIn = async () => {
  const isSignedIn = await GoogleSignin.isSignedIn();
  this.props.navigation.navigate('Menu');
};

  _getCurrentUser = async () => {
    //May be called eg. in the componentDidMount of your main component.
    //This method returns the current user
    //if they already signed in and null otherwise.
    try {
      const userInfo = await GoogleSignin.signInSilently();
      this.setState({ userInfo });
    } catch (error) {
      console.error(error);
    }
  };


  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <ImageBackground 
          source={require('./images/back.jpg')}
          style={{flex:1,alignItems:'center',}} 
        >

          <Image style={styles.image} source={require('./images/login.png')}/>
         
            <Modal animationType={'slide'} 
              transparent = {true} visible = {this.state.visible1}>
               <ImageBackground 
    source={require('./images/back.jpg')}
    style={{width: 350,
          height: 200,
          marginTop:230,alignSelf:'center',
            alignItems:'center',
            justifyContent:'center'}} 
               >
                <View style={{
                  width: 350,
                  height: 200,
                  backgroundColor:'#transparent',
                  alignSelf:'center',
                  borderColor:'#000',
                  borderWidth:3}}
                >
                  <Text style={styles.textmodal}>Login inválido. Por favor, insira um usuário e/ou uma senha.</Text>

                  <TouchableOpacity style={styles.buttonmodal} onPress={this.handleCancel1}>
                    <Text style={styles.textbuttonmodal}> OK </Text>
                  </TouchableOpacity>
    
                </View>
                </ImageBackground>
            </Modal>


            <Modal animationType={'slide'} transparent = {true} visible = {this.state.visible2}>
             <ImageBackground 
    source={require('./images/back.jpg')}
    style={{width: 350,
          height: 200,
          marginTop:230,alignSelf:'center',
            alignItems:'center',
            justifyContent:'center'}} 
               >
                <View style={{
                  width: 350,
                  height: 200,
                  backgroundColor:'#transparent',
                  alignSelf:'center',
                  borderColor:'#000',
                  borderWidth:3}}>

                  <Text style={styles.textmodal}>'Login inválido. Por favor, insira um usuário e/ou uma senha válido(s).'</Text>

                  <TouchableOpacity style={styles.buttonmodal2} onPress={this.handleCancel2}>
                    <Text style={styles.textbuttonmodal}>OK</Text>
                  </TouchableOpacity>

                </View>
                </ImageBackground>
            </Modal>




       
          <Text style={styles.textlogin}>Entrar com e-mail e senha cadastrados no Saúde+</Text>

            <TextInput style = {styles.input}
              underlineColorAndroid = "transparent"
              placeholder = "E-mail:"
              placeholderTextColor = "#000"
              autoCapitalize = "none"
              keyboardType='email-address'
              onChangeText={(email) => this.setState({email})}
              value={this.state.email}
            />

            <TextInput style = {styles.input2}
              underlineColorAndroid = "transparent"
              placeholder = "Senha:"
              placeholderTextColor = "#000"
              autoCapitalize = "none"
              keyboardType='default'
              onChangeText={(password) => this.setState({password})}
              value={this.state.password}
              Textinput secureTextEntry={true} 
              />  

            <Text style={styles.textesqueceusenha} onPress={() => this.props.navigation.navigate('MudarSenha')}>Esqueceu a senha?</Text>

            <TouchableOpacity style={styles.button} onPress={this._onPressButton}>
              <Text style={styles.textButton} onPress={this.handleLogin}> Entrar  </Text>
            </TouchableOpacity>

            <Text style={styles.textlogin}>Outras opções:</Text>

            <LoginButton style={styles.button4}
              onLoginFinished={
                (error, result) => {
                  if (error) {
                    alert("O login possui o seguinte erro:" + result.error);
                  } else if (result.isCancelled) {
                    alert("Login cancelado pelo usuário.");
                  } else {
                    AccessToken.getCurrentAccessToken().then(
                      (data) => {
                        alert('Logado com sucesso')
                        this.props.navigation.navigate('Menu')
                      }
                    )
                   }
                }
              } 

              onLogoutFinished={() => alert('Deslogado com sucesso.')} />

            <GoogleSigninButton
              style={{ width: 312, height: 48, marginTop:5, }}
              size={GoogleSigninButton.Size.Wide}
              color={GoogleSigninButton.Color.Dark}
              onPress={this._signIn}
            />

        </ImageBackground>  
               
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ACCEE8',
    flex: 1,
    justifyContent:'center'
  },
  containermodal: {
    backgroundColor: '#FFDBD9',
    justifyContent:'center',
    alignItems:'center'
  },
  inner: {
    justifyContent: 'center', 
    alignItems: 'center'
  },
  image: { 
    width:  200, 
    height: 200,
    alignItems:'center',
    marginTop:50
  },
  input: {
    height: 50,
    width: 300,
    backgroundColor: '#FF7A7D',
    padding: 10,
    color: '#000',
    margin:15,
    marginTop:-30,
    borderRadius:20,
    fontSize:20
   },
  input2: {
    height: 50,
    width: 300,
    backgroundColor: '#FF7A7D',
    padding: 10,
    color: '#000',
    margin:15,
    marginTop:0,
    borderRadius:20,
    fontSize:20
   },
  button4: {
    height: 45,
    width: 305,
    marginTop:-55
  },
  button: {
    height: 45,
    width: 140,
    padding: 15,
    backgroundColor: '#FF7A7D',
    margin:10,
    borderColor: '#ff6505', 
    borderRadius:25,
    left:70,
    top:-28,
    borderColor:'#fff',
    borderWidth:2
  },
  button2: {
    height: 45,
    width: 140,
    padding: 15,
    backgroundColor: '#FFFFFF',
    borderRadius:25,
    marginTop:-55,
  },
  buttonmodal:{
    height: 35,
    width: 110,
    padding: 15,
    backgroundColor: '#FF7A7D',
    margin:15,
    borderColor: '#FF7A7D',
    alignSelf:'flex-end',
    marginTop:65,
    borderColor:'#fff',
    borderWidth:2,
  },
  buttonmodal2:{
    height: 30,
    width: 100,
    padding: 15,
    backgroundColor: '#FF7A7D',
    margin:15,
    borderColor: '#FF7A7D',
    alignSelf:'flex-end',
    marginTop:70,
    borderColor:'#fff',
    borderWidth:2,
  },
  textmodal: {
    color: '#000',
    fontWeight: '700',
    fontSize: 25
  },
  textbuttonmodal: {
    color: '#000',
    fontWeight: '700',
    fontSize: 20,
    marginTop:-13,
    left:17
  },
  textesqueceusenha: {
    fontSize: 14,      
    color: '#000',
    fontWeight: '800',
    textDecorationLine:'underline',
    top:14,
    right:70
},
  textButton: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 19,
    marginTop:-7
  },
  textlogin: {
    height: 100,
    margin:5,
    padding:10,
    fontSize: 20,
    marginTop:-41,
    alignItems:'center',
    justifyContent:'center',
    color: '#000',
    textAlign: 'center',
    fontWeight: '900',
    fontSize: 19
}

});

 export default Page11;