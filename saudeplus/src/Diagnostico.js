import React, { Component } from 'react';
import { Text, View, Image, TextInput, TouchableOpacity, StyleSheet ,
  Alert,ScrollView, FlatList, ActivityIndicator,Modal,ImageBackground} from 'react-native';
import firebase from 'firebase';


 class Page5 extends Component {
   static navigationOptions = {
    header: null,
  };
  
state = {apatiax:false,ardorlocalizadox:false,angustiax:false,ansiedadex:false,apetiteemexcessox:false,
  bolhaslocalizadasx:false,cansacox:false,chiadonopeitox:false,coceirax:false,
  colicasx:false,diarreiax:false,dorabdominalx:false,dordecabecax:false,dorfacialx:false,
  dordeouvidox:false,doraoengolirx:false,dordegargantax:false,dormuscularx:false,dornopeitox:false,
  fadigax:false,faltadearfrequentex:false,faltadeapetitex:false,fraquezax:false,febrex:false,fotofobiax:false,
  fonofobiax:false,gasesx:false,gargantasecax:false,gargantainchadax:false,indisposicaox:false,
  irritabilidadex:false,insegurancax:false,malestarx:false,mauhalitox:false,manchasvermelhasx:false,
  nauseax:false,obstrucaonasalx:false,palidezx:false,perdadepesox:false,peleamareladax:false,
  sangramentonasalx:false,sonolenciax:false,sonoirregularx:false,sedeemexcessox:false,tonturax:false,
  tossesecax:false,ulcerasx:false,urinaescurax:false,urinaemexcessox:false,vistaembacadax:false,
  vomitosx:false,
  visible1:false,visible2:false,visible3:false,visible4:false,visible5:false,
  visible6:false,visible7:false,visible8:false,visible9:false,visible10:false,visible11:false,
  visible12:false,visible13:false,visible14:false,visible15:false,visible16:false,visible17:false,
  visible18:false,visible19:false,visible20:false,visible21:false,visible22:false,visible23:false
,visible24:false,visible25:false,visible26:false}

apatia = () => {    
      this.setState({ apatiax: true });
      Alert.alert("Apatia marcado");
    }
    
ardorlocalizado = () => {
      this.setState({ ardorlocalizadox: true });
      Alert.alert("Ardor Localizado marcado");
    }

angustia = () => {
      this.setState({ angustiax: true });
      Alert.alert("Angústia marcado");
    }

ansiedade = () => {
      this.setState({ ansiedadex: true });
      Alert.alert("Ansiedade marcado");
    }   

apetiteemexcesso = () => {
      this.setState({ apetiteemexcessox: true });
      Alert.alert("Apetite em excesso marcado");
    }   


bolhaslocalizadas = () => {
      this.setState({ bolhaslocalizadasx: true });
      Alert.alert("Bolhas localizadas marcado");
    }   

cansaco = () => {
      this.setState({ cansacox: true });
      Alert.alert("Cansaço marcado");
    }

chiadonopeito = () => {
      this.setState({ chiadonopeitox: true });
      Alert.alert("Chiado no peito marcado");
    }   

coceira = () => {
      this.setState({coceirax: true });
      Alert.alert("Coceira marcado");
    }  

colicas = () => {
      this.setState({ colicasx: true });
      Alert.alert("Cólicas marcado");
    }


diarreia = () => {
      this.setState({ diarreiax: true });
      Alert.alert("Diarréia marcado");
    }

dorabdominal = () => {
      this.setState({ dorabdominalx: true });
      Alert.alert("Dor abdominal marcado");
    }

dordecabeca = () => {
      this.setState({ dordecabecax: true });
      Alert.alert("Dor de cabeça marcado");
    }

dorfacial = () => {
      this.setState({ dorfacialx: true });
      Alert.alert("Dor facial marcado");
    }

dordeouvido = () => {
      this.setState({ dordeouvidox: true });
      Alert.alert("Dor de ouvido marcado");
    }


doraoengolir = () => {
      this.setState({ doraoengolirx: true });
      Alert.alert("Dor ao engolir marcado");
    }

dordegarganta = () => {
      this.setState({ dordegargantax: true });
      Alert.alert("Dor de garganta marcado");
    }

dormuscular = () => {
      this.setState({ dormuscularx: true });
      Alert.alert("Dor muscular marcado");
    }

dornopeito = () => {
      this.setState({ dornopeitox: true });
      Alert.alert("Dor no peito marcado");
    }

fadiga = () => {
      this.setState({ fadigax: true });
      Alert.alert("Fadiga marcado");
    }

faltadearfrequente = () => {
      this.setState({ faltadearfrequentex: true });
      Alert.alert("Falta de ar frequente marcado");
    }


faltadeapetite = () => {
      this.setState({ faltadeapetitex: true });
      Alert.alert("Falta de apetite marcado");
    }

fraqueza = () => {
      this.setState({ fraquezax: true });
      Alert.alert("Fraqueza marcado");
    }   

febre = () => {
    this.setState({ febrex: true });
    Alert.alert("Febre marcado");      
    } 

fotofobia = () => {
    this.setState({ fotofobiax: true });
    Alert.alert("Fotofobia marcado");      
    } 

fonofobia = () => {
    this.setState({ fonofobiax: true });
    Alert.alert("Fonofobia marcado");      
    } 

gases = () => {
    this.setState({ gasesx: true });
    Alert.alert("Gases marcado");      
    } 

gargantaseca = () => {
    this.setState({ gargantasecax: true });
    Alert.alert("Garganta seca marcado");      
    } 


gargantainchada = () => {
    this.setState({ gargantainchadax: true });
    Alert.alert("Garganta inchada marcado");      
    } 

indisposicao = () => {
    this.setState({ indisposicaox: true });
    Alert.alert("Indisposição marcado");      
    } 


irritabilidade = () => {
      this.setState({ irritabilidadex: true });
      Alert.alert("Irritabilidade marcado");
    }

inseguranca = () => {
      this.setState({ insegurancax: true });
      Alert.alert("Insegurança marcado");
    }

malestar = () => {
      this.setState({ malestarx: true });
      Alert.alert("Mal-estar marcado");
    }

mauhalito = () => {
      this.setState({ mauhalitox: true });
      Alert.alert("Mau hálito marcado");
    }   

manchasvermelhas = () => {
        this.setState({ manchasvermelhasx: true });
        Alert.alert("Manchas vermelhas marcado");
    }


nausea = () => {
        this.setState({ nauseax: true });
        Alert.alert("Náuseas marcado");
    }

obstrucaonasal = () => {
        this.setState({ obstrucaonasalx: true });
        Alert.alert("Obstrução nasal marcado");
    }

palidez = () => {
        this.setState({ palidezx: true });
        Alert.alert("Palidez marcado");
    }

perdadepeso = () => {
        this.setState({ perdadepesox: true });
        Alert.alert("Perda de peso marcado");
    }

peleamarelada = () => {
        this.setState({ peleamareladax: true });
        Alert.alert("Pele amarelada marcado");
    }


sangramentonasal = () => {
        this.setState({ sangramentonasalx: true });
        Alert.alert("Sangramento nasal marcado");
    }

sonolencia = () => {
        this.setState({ sonolenciax: true });
        Alert.alert("Sonolência marcado");
    }

sonoirregular = () => {
        this.setState({ sonoirregularx: true });
        Alert.alert("Sono irregular marcado");
    }

sedeemexcesso = () => {
        this.setState({ sedeemexcessox: true });
        Alert.alert("Sede em excesso marcado");
    }

tontura = () => {
      this.setState({ tonturax: true });
      Alert.alert("Tontura marcado");
    }

tosseseca = () => {
      this.setState({ tossesecax: true });
      Alert.alert("Tosse seca marcado");
    }

ulceras = () => {
      this.setState({ ulcerasx: true });
      Alert.alert("Úlceras marcado");
    }


urinaescura = () => {
      this.setState({ urinaescurax: true });
      Alert.alert("Urina escura marcado");
    }

urinaemexcesso = () => {
      this.setState({ urinaemexcesso: true });
      Alert.alert("Urina em excesso marcado");
    }

vistaembacada = () => {
      this.setState({ vistaembacadax: true });
      Alert.alert("Vista embaçada marcado");
    }

vomitos = () => {
        this.setState({ vomitosx: true });
        Alert.alert("Vômitos marcado");
    }

desmarcartudo = () => {
    
    this.setState({ apatiax: false });
    this.setState({ ardorlocalizadox: false });
    this.setState({ angustiax: false });
    this.setState({ ansiedadex: false });
    this.setState({ apetiteemexcessox: false });
    this.setState({ bolhaslocalizadasx: false });
    this.setState({ cansacox: false });
    this.setState({ chiadonopeitox: false });
    this.setState({coceirax: false });
    this.setState({ colicasx: false });
    this.setState({ diarreiax: false });
    this.setState({ dorabdominalx: false });
    this.setState({ dordecabecax: false });
    this.setState({ dorfacialx: false });
     this.setState({ dordeouvidox: false });
     this.setState({ doraoengolirx: false });
     this.setState({ dordegargantax: false });
     this.setState({ dormuscularx: false });
     this.setState({ dornopeitox: false });
     this.setState({ fadigax: false });
     this.setState({ faltadearfrequentex: false });
     this.setState({ faltadeapetitex: false });
     this.setState({ fraquezax: false });
     this.setState({ febrex: false });
     this.setState({ fotofobiax: false });
     this.setState({ fonofobiax: false });
     this.setState({ gasesx: false });
     this.setState({ gargantasecax: false });
     this.setState({ gargantainchadax: false });
     this.setState({ indisposicaox: false });
     this.setState({ irritabilidadex: false });
     this.setState({ insegurancax: false });
     this.setState({ malestarx: false });
     this.setState({ mauhalitox: false });
     this.setState({ manchasvermelhasx: false });
     this.setState({ nauseax: false });
     this.setState({ obstrucaonasalx: false });
     this.setState({ palidezx: false });
     this.setState({ perdadepesox: false });
     this.setState({ peleamareladax: false });
     this.setState({ sangramentonasalx: false });
     this.setState({ sonolenciax: false });
     this.setState({ sonoirregularx: false });
     this.setState({ sedeemexcessox: false });
     this.setState({ tonturax: false });
     this.setState({ tossesecax: false });
     this.setState({ ulcerasx: false });
     this.setState({ urinaescurax: false });
     this.setState({ urinaemexcesso: false });
     this.setState({ vistaembacadax: false });
     this.setState({ vomitosx: false });

      Alert.alert("desmarcado com sucesso");
    }

 cancelargripegrande = () => {
    this.setState({ visible1: false });
  }

   cancelargripemedia = () => {
    this.setState({ visible2: false });
  }
  cancelarsinusitemedia = () => {
    this.setState({ visible3: false });
  }

  cancelarsinusitepequena = () => {
    this.setState({ visible4: false });
  }
cancelaranemiamedia = () => {
    this.setState({ visible5: false });
  }

  cancelaranemiapequena = () => {
    this.setState({ visible6: false });
  }
  cancelarfaringitegrande = () => {
    this.setState({ visible7: false });
  }

  cancelarfaringitepequena = () => {
    this.setState({ visible8: false });
  }
  cancelarinfeccaointestinalmedia= () => {
    this.setState({ visible9: false });
  }
  cancelarenxaquecagrande = () => {
    this.setState({ visible10: false });
  }

    cancelarenxaquecapequena = () => {
    this.setState({ visible11: false });
  }

    cancelarasmamedia = () => {
    this.setState({ visible12: false });
  }

   cancelardepressaomedia = () => {
    this.setState({ visible13: false });
  }

   cancelardepressaopequena = () => {
    this.setState({ visible14: false });
  }

    cancelardiabetespequena = () => {
    this.setState({ visible15: false });
  }

  cancelarpressaoaltamedia = () => {
    this.setState({ visible16: false });
  }

  cancelarpressaoaltapequena = () => {
    this.setState({ visible17: false });
  }

  cancelardenguegrande = () => {
    this.setState({ visible18: false });
  }

   cancelardenguemedia = () => {
    this.setState({ visible19: false });
  }

   cancelardenguepequena = () => {
    this.setState({ visible20: false });
  }

   cancelarinsoniagrande = () => {
    this.setState({ visible21: false });
  }

   cancelarherpesgrande = () => {
    this.setState({ visible22: false });
  }

  cancelarcaxumbagrande = () => {
    this.setState({ visible23: false });
  }

  cancelarhepatitecmedia = () => {
    this.setState({ visible24: false });
  }

  cancelarhepatitecpequena = () => {
    this.setState({ visible25: false });
  }

  cancelarnenhumsintoma = () => {
    this.setState({ visible26: false });
  }
   


  diagnostico = () =>{ let {apatiax,ardorlocalizadox,angustiax,ansiedadex,apetiteemexcessox,
  bolhaslocalizadasx,cansacox,chiadonopeitox,coceirax,
  colicasx,diarreiax,dorabdominalx,dordecabecax,dorfacialx,
  dordeouvidox,doraoengolirx,dordegargantax,dormuscularx,dornopeitox,
  fadigax,faltadearfrequentex,faltadeapetitex,fraquezax,febrex,fotofobiax,
  fonofobiax,gasesx,gargantasecax,gargantainchadax,indisposicaox,
  irritabilidadex,insegurancax,malestarx,mauhalitox,manchasvermelhasx,
  nauseax,obstrucaonasalx,palidezx,perdadepesox,peleamareladax,
  sangramentonasalx,sonolenciax,sonoirregularx,sedeemexcessox,tonturax,
  tossesecax,ulcerasx,urinaescurax,urinaemexcessox,vistaembacadax,
  vomitosx,esmarcartudo} = this.state;

      

      if( (febrex==true) || (dordecabecax==true)|| 
          (tossesecax==true)|| (fraquezax==true)||
          (obstrucaonasalx==true) ){



              if( (febrex==true) && (dordecabecax==true) && (tossesecax==true)||
                (febrex==true) && (dordecabecax==true) && (fraquezax==true)||
                (febrex==true) && (dordecabecax==true)&&(obstrucaonasalx==true) ||
                (dordecabecax==true) && (tossesecax==true) && (fraquezax==true) ||
                (dordecabecax==true) && (tossesecax==true) && (obstrucaonasalx==true) ||
                (dordecabecax==true) && (fraquezax==true) && (obstrucaonasalx==true) ||
                (tossesecax==true) && (febrex==true) && (fraquezax==true) ||
                (tossesecax==true) && (febrex==true) && (obstrucaonasalx==true) ||
                (tossesecax==true) && (fraquezax==true) && (obstrucaonasalx==true) ||
                (fraquezax==true) && (febrex==true) && (obstrucaonasalx==true)){
              this.setState({ visible1: true });//gripegrande

            this.setState({ apatiax: false });
    this.setState({ ardorlocalizadox: false });
    this.setState({ angustiax: false });
    this.setState({ ansiedadex: false });
    this.setState({ apetiteemexcessox: false });
    this.setState({ bolhaslocalizadasx: false });
    this.setState({ cansacox: false });
    this.setState({ chiadonopeitox: false });
    this.setState({coceirax: false });
    this.setState({ colicasx: false });
    this.setState({ diarreiax: false });
    this.setState({ dorabdominalx: false });
    this.setState({ dordecabecax: false });
    this.setState({ dorfacialx: false });
     this.setState({ dordeouvidox: false });
     this.setState({ doraoengolirx: false });
     this.setState({ dordegargantax: false });
     this.setState({ dormuscularx: false });
     this.setState({ dornopeitox: false });
     this.setState({ fadigax: false });
     this.setState({ faltadearfrequentex: false });
     this.setState({ faltadeapetitex: false });
     this.setState({ fraquezax: false });
     this.setState({ febrex: false });
     this.setState({ fotofobiax: false });
     this.setState({ fonofobiax: false });
     this.setState({ gasesx: false });
     this.setState({ gargantasecax: false });
     this.setState({ gargantainchadax: false });
     this.setState({ indisposicaox: false });
     this.setState({ irritabilidadex: false });
     this.setState({ insegurancax: false });
     this.setState({ malestarx: false });
     this.setState({ mauhalitox: false });
     this.setState({ manchasvermelhasx: false });
     this.setState({ nauseax: false });
     this.setState({ obstrucaonasalx: false });
     this.setState({ palidezx: false });
     this.setState({ perdadepesox: false });
     this.setState({ peleamareladax: false });
     this.setState({ sangramentonasalx: false });
     this.setState({ sonolenciax: false });
     this.setState({ sonoirregularx: false });
     this.setState({ sedeemexcessox: false });
     this.setState({ tonturax: false });
     this.setState({ tossesecax: false });
     this.setState({ ulcerasx: false });
     this.setState({ urinaescurax: false });
     this.setState({ urinaemexcesso: false });
     this.setState({ vistaembacadax: false });
     this.setState({ vomitosx: false });

              
                }

                else if( (febrex==true) && (dordecabecax==true)|| (febrex==true) && 
                (tossesecax==true)||(febrex==true) && (fraquezax==true)||(febrex==true)
                 && (obstrucaonasalx==true) || (tossesecax==true) && (dordecabecax==true) 
                 || (tossesecax==true) && (fraquezax==true)|| (tossesecax==true) && (obstrucaonasalx==true)
                 || (dordecabecax==true) && (fraquezax==true)|| (dordecabecax==true) && (obstrucaonasalx==true)
                 || (fraquezax==true) && (obstrucaonasalx==true)) {
                this.setState({ visible2: true });//gripemedia

              this.setState({ apatiax: false });
    this.setState({ ardorlocalizadox: false });
    this.setState({ angustiax: false });
    this.setState({ ansiedadex: false });
    this.setState({ apetiteemexcessox: false });
    this.setState({ bolhaslocalizadasx: false });
    this.setState({ cansacox: false });
    this.setState({ chiadonopeitox: false });
    this.setState({coceirax: false });
    this.setState({ colicasx: false });
    this.setState({ diarreiax: false });
    this.setState({ dorabdominalx: false });
    this.setState({ dordecabecax: false });
    this.setState({ dorfacialx: false });
     this.setState({ dordeouvidox: false });
     this.setState({ doraoengolirx: false });
     this.setState({ dordegargantax: false });
     this.setState({ dormuscularx: false });
     this.setState({ dornopeitox: false });
     this.setState({ fadigax: false });
     this.setState({ faltadearfrequentex: false });
     this.setState({ faltadeapetitex: false });
     this.setState({ fraquezax: false });
     this.setState({ febrex: false });
     this.setState({ fotofobiax: false });
     this.setState({ fonofobiax: false });
     this.setState({ gasesx: false });
     this.setState({ gargantasecax: false });
     this.setState({ gargantainchadax: false });
     this.setState({ indisposicaox: false });
     this.setState({ irritabilidadex: false });
     this.setState({ insegurancax: false });
     this.setState({ malestarx: false });
     this.setState({ mauhalitox: false });
     this.setState({ manchasvermelhasx: false });
     this.setState({ nauseax: false });
     this.setState({ obstrucaonasalx: false });
     this.setState({ palidezx: false });
     this.setState({ perdadepesox: false });
     this.setState({ peleamareladax: false });
     this.setState({ sangramentonasalx: false });
     this.setState({ sonolenciax: false });
     this.setState({ sonoirregularx: false });
     this.setState({ sedeemexcessox: false });
     this.setState({ tonturax: false });
     this.setState({ tossesecax: false });
     this.setState({ ulcerasx: false });
     this.setState({ urinaescurax: false });
     this.setState({ urinaemexcesso: false });
     this.setState({ vistaembacadax: false });
     this.setState({ vomitosx: false });


              }

      
    }
      if( (dorfacialx==true) || (nauseax==true)|| 
                  (dordeouvidox==true)|| (mauhalitox==true)||
                  (obstrucaonasalx==true)){

                      if( (dorfacialx==true) && (nauseax==true) && (dordeouvidox==true)||
                (dorfacialx==true) && (nauseax==true) && (mauhalitox==true)||
                (dorfacialx==true) && (nauseax==true)&&(obstrucaonasalx==true) ||
                (nauseax==true) && (dordeouvidox==true) && (mauhalitox==true) ||
                (nauseax==true) && (dordeouvidox==true) && (obstrucaonasalx==true) ||
                (nauseax==true) && (mauhalitox==true) && (obstrucaonasalx==true) ||
                (dordeouvidox==true) && (dorfacialx==true) && (mauhalitox==true) ||
                (dordeouvidox==true) && (dorfacialx==true) && (obstrucaonasalx==true) ||
                (dordeouvidox==true) && (mauhalitox==true) && (obstrucaonasalx==true) ||
                (mauhalitox==true) && (dorfacialx=true) && (obstrucaonasalx==true)){
              this.setState({ visible3: true });//sinusite média
            this.setState({ apatiax: false });
    this.setState({ ardorlocalizadox: false });
    this.setState({ angustiax: false });
    this.setState({ ansiedadex: false });
    this.setState({ apetiteemexcessox: false });
    this.setState({ bolhaslocalizadasx: false });
    this.setState({ cansacox: false });
    this.setState({ chiadonopeitox: false });
    this.setState({coceirax: false });
    this.setState({ colicasx: false });
    this.setState({ diarreiax: false });
    this.setState({ dorabdominalx: false });
    this.setState({ dordecabecax: false });
    this.setState({ dorfacialx: false });
     this.setState({ dordeouvidox: false });
     this.setState({ doraoengolirx: false });
     this.setState({ dordegargantax: false });
     this.setState({ dormuscularx: false });
     this.setState({ dornopeitox: false });
     this.setState({ fadigax: false });
     this.setState({ faltadearfrequentex: false });
     this.setState({ faltadeapetitex: false });
     this.setState({ fraquezax: false });
     this.setState({ febrex: false });
     this.setState({ fotofobiax: false });
     this.setState({ fonofobiax: false });
     this.setState({ gasesx: false });
     this.setState({ gargantasecax: false });
     this.setState({ gargantainchadax: false });
     this.setState({ indisposicaox: false });
     this.setState({ irritabilidadex: false });
     this.setState({ insegurancax: false });
     this.setState({ malestarx: false });
     this.setState({ mauhalitox: false });
     this.setState({ manchasvermelhasx: false });
     this.setState({ nauseax: false });
     this.setState({ obstrucaonasalx: false });
     this.setState({ palidezx: false });
     this.setState({ perdadepesox: false });
     this.setState({ peleamareladax: false });
     this.setState({ sangramentonasalx: false });
     this.setState({ sonolenciax: false });
     this.setState({ sonoirregularx: false });
     this.setState({ sedeemexcessox: false });
     this.setState({ tonturax: false });
     this.setState({ tossesecax: false });
     this.setState({ ulcerasx: false });
     this.setState({ urinaescurax: false });
     this.setState({ urinaemexcesso: false });
     this.setState({ vistaembacadax: false });
     this.setState({ vomitosx: false });

                }

                else if( (dorfacialx==true) && (nauseax==true)|| (dorfacialx==true) && 
                (dordeouvidox==true)||(dorfacialx==true) && (mauhalitox==true)||(dorfacialx==true)
                 && (obstrucaonasalx==true) || (dordeouvidox==true) && (nauseax==true) 
                 || (dordeouvidox==true) && (mauhalitox==true)|| (dordeouvidox==true) && (obstrucaonasalx==true)
                 || (nauseax==true) && (mauhalitox==true)|| (nauseax==true) && (obstrucaonasalx==true)
                 || (mauhalitox==true) && (obstrucaonasalx==true)) {
                this.setState({ visible4: true });//sinusite pequena
              this.setState({ apatiax: false });
    this.setState({ ardorlocalizadox: false });
    this.setState({ angustiax: false });
    this.setState({ ansiedadex: false });
    this.setState({ apetiteemexcessox: false });
    this.setState({ bolhaslocalizadasx: false });
    this.setState({ cansacox: false });
    this.setState({ chiadonopeitox: false });
    this.setState({coceirax: false });
    this.setState({ colicasx: false });
    this.setState({ diarreiax: false });
    this.setState({ dorabdominalx: false });
    this.setState({ dordecabecax: false });
    this.setState({ dorfacialx: false });
     this.setState({ dordeouvidox: false });
     this.setState({ doraoengolirx: false });
     this.setState({ dordegargantax: false });
     this.setState({ dormuscularx: false });
     this.setState({ dornopeitox: false });
     this.setState({ fadigax: false });
     this.setState({ faltadearfrequentex: false });
     this.setState({ faltadeapetitex: false });
     this.setState({ fraquezax: false });
     this.setState({ febrex: false });
     this.setState({ fotofobiax: false });
     this.setState({ fonofobiax: false });
     this.setState({ gasesx: false });
     this.setState({ gargantasecax: false });
     this.setState({ gargantainchadax: false });
     this.setState({ indisposicaox: false });
     this.setState({ irritabilidadex: false });
     this.setState({ insegurancax: false });
     this.setState({ malestarx: false });
     this.setState({ mauhalitox: false });
     this.setState({ manchasvermelhasx: false });
     this.setState({ nauseax: false });
     this.setState({ obstrucaonasalx: false });
     this.setState({ palidezx: false });
     this.setState({ perdadepesox: false });
     this.setState({ peleamareladax: false });
     this.setState({ sangramentonasalx: false });
     this.setState({ sonolenciax: false });
     this.setState({ sonoirregularx: false });
     this.setState({ sedeemexcessox: false });
     this.setState({ tonturax: false });
     this.setState({ tossesecax: false });
     this.setState({ ulcerasx: false });
     this.setState({ urinaescurax: false });
     this.setState({ urinaemexcesso: false });
     this.setState({ vistaembacadax: false });
     this.setState({ vomitosx: false });

              }

              }
      if( (palidezx==true) || (fadigax==true)|| 
                  (dormuscularx==true)|| (faltadeapetitex==true)||
                  (indisposicaox==true)){

              if( (palidezx==true) && (fadigax==true) && (dormuscularx==true)||
                    (palidezx==true) && (fadigax==true) && (faltadeapetitex==true)||
                    (palidezx==true) && (fadigax==true)&&(indisposicaox==true) ||
                    (fadigax==true) && (dormuscularx==true) && (faltadeapetitex==true) ||
                    (fadigax==true) && (dormuscularx==true) && (indisposicaox==true) ||
                    (fadigax==true) && (faltadeapetitex==true) && (indisposicaox==true) ||
                    (dormuscularx==true) && (palidezx==true) && (faltadeapetitex==true) ||
                    (dormuscularx==true) && (palidezx==true) && (indisposicaox==true) ||
                    (dordeouvidox==true) && (faltadeapetitex==true) && (indisposicaox==true) ||
                    (faltadeapetitex==true) && (palidezx=true) && (indisposicaox==true)){
                  this.setState({ visible5: true });//anemia média
                this.setState({ apatiax: false });
    this.setState({ ardorlocalizadox: false });
    this.setState({ angustiax: false });
    this.setState({ ansiedadex: false });
    this.setState({ apetiteemexcessox: false });
    this.setState({ bolhaslocalizadasx: false });
    this.setState({ cansacox: false });
    this.setState({ chiadonopeitox: false });
    this.setState({coceirax: false });
    this.setState({ colicasx: false });
    this.setState({ diarreiax: false });
    this.setState({ dorabdominalx: false });
    this.setState({ dordecabecax: false });
    this.setState({ dorfacialx: false });
     this.setState({ dordeouvidox: false });
     this.setState({ doraoengolirx: false });
     this.setState({ dordegargantax: false });
     this.setState({ dormuscularx: false });
     this.setState({ dornopeitox: false });
     this.setState({ fadigax: false });
     this.setState({ faltadearfrequentex: false });
     this.setState({ faltadeapetitex: false });
     this.setState({ fraquezax: false });
     this.setState({ febrex: false });
     this.setState({ fotofobiax: false });
     this.setState({ fonofobiax: false });
     this.setState({ gasesx: false });
     this.setState({ gargantasecax: false });
     this.setState({ gargantainchadax: false });
     this.setState({ indisposicaox: false });
     this.setState({ irritabilidadex: false });
     this.setState({ insegurancax: false });
     this.setState({ malestarx: false });
     this.setState({ mauhalitox: false });
     this.setState({ manchasvermelhasx: false });
     this.setState({ nauseax: false });
     this.setState({ obstrucaonasalx: false });
     this.setState({ palidezx: false });
     this.setState({ perdadepesox: false });
     this.setState({ peleamareladax: false });
     this.setState({ sangramentonasalx: false });
     this.setState({ sonolenciax: false });
     this.setState({ sonoirregularx: false });
     this.setState({ sedeemexcessox: false });
     this.setState({ tonturax: false });
     this.setState({ tossesecax: false });
     this.setState({ ulcerasx: false });
     this.setState({ urinaescurax: false });
     this.setState({ urinaemexcesso: false });
     this.setState({ vistaembacadax: false });
     this.setState({ vomitosx: false });

                    }

              else if( (palidezx==true) && (fadigax==true)|| (palidezx==true) && 
                      (dormuscularx==true)||(palidezx==true) && (faltadeapetitex==true)||(palidezx==true)
                       && (indisposicaox==true) || (dormuscularx==true) && (fadigax==true) 
                       || (dormuscularx==true) && (faltadeapetitex==true)|| (dormuscularx==true) && (indisposicaox==true)
                       || (fadigax==true) && (faltadeapetitex==true)|| (fadigax==true) && (indisposicaox==true)
                       || (faltadeapetitex==true) && (indisposicaox==true)) {
                      this.setState({ visible6: true });//anemia pequena
                    this.setState({ apatiax: false });
    this.setState({ ardorlocalizadox: false });
    this.setState({ angustiax: false });
    this.setState({ ansiedadex: false });
    this.setState({ apetiteemexcessox: false });
    this.setState({ bolhaslocalizadasx: false });
    this.setState({ cansacox: false });
    this.setState({ chiadonopeitox: false });
    this.setState({coceirax: false });
    this.setState({ colicasx: false });
    this.setState({ diarreiax: false });
    this.setState({ dorabdominalx: false });
    this.setState({ dordecabecax: false });
    this.setState({ dorfacialx: false });
     this.setState({ dordeouvidox: false });
     this.setState({ doraoengolirx: false });
     this.setState({ dordegargantax: false });
     this.setState({ dormuscularx: false });
     this.setState({ dornopeitox: false });
     this.setState({ fadigax: false });
     this.setState({ faltadearfrequentex: false });
     this.setState({ faltadeapetitex: false });
     this.setState({ fraquezax: false });
     this.setState({ febrex: false });
     this.setState({ fotofobiax: false });
     this.setState({ fonofobiax: false });
     this.setState({ gasesx: false });
     this.setState({ gargantasecax: false });
     this.setState({ gargantainchadax: false });
     this.setState({ indisposicaox: false });
     this.setState({ irritabilidadex: false });
     this.setState({ insegurancax: false });
     this.setState({ malestarx: false });
     this.setState({ mauhalitox: false });
     this.setState({ manchasvermelhasx: false });
     this.setState({ nauseax: false });
     this.setState({ obstrucaonasalx: false });
     this.setState({ palidezx: false });
     this.setState({ perdadepesox: false });
     this.setState({ peleamareladax: false });
     this.setState({ sangramentonasalx: false });
     this.setState({ sonolenciax: false });
     this.setState({ sonoirregularx: false });
     this.setState({ sedeemexcessox: false });
     this.setState({ tonturax: false });
     this.setState({ tossesecax: false });
     this.setState({ ulcerasx: false });
     this.setState({ urinaescurax: false });
     this.setState({ urinaemexcesso: false });
     this.setState({ vistaembacadax: false });
     this.setState({ vomitosx: false });

                    }

      }

   else   if( (doraoengolirx==true) || (gargantasecax==true)|| 
                  (dordegargantax==true)|| (dordeouvidox==true)||
                  (febrex==true)){

             if( (doraoengolirx==true) && (gargantasecax==true) && (dordegargantax==true)||
                    (doraoengolirx==true) && (gargantasecax==true) && (dordeouvidox==true)||
                    (doraoengolirx==true) && (gargantasecax==true)&&(febrex==true) ||
                    (gargantasecax==true) && (dordegargantax==true) && (dordeouvidox==true) ||
                    (gargantasecax==true) && (dordegargantax==true) && (febrex==true) ||
                    (gargantasecax==true) && (dordeouvidox==true) && (febrex==true) ||
                    (dordegargantax==true) && (doraoengolirx==true) && (dordeouvidox==true) ||
                    (dordegargantax==true) && (doraoengolirx==true) && (febrex==true) ||
                    (dordegargantax==true) && (dordeouvidox==true) && (febrex==true) ||
                    (dordeouvidox==true) && (doraoengolirx=true) && (febrex==true)){
                  this.setState({ visible7: true });//faringite grande
                this.setState({ apatiax: false });
    this.setState({ ardorlocalizadox: false });
    this.setState({ angustiax: false });
    this.setState({ ansiedadex: false });
    this.setState({ apetiteemexcessox: false });
    this.setState({ bolhaslocalizadasx: false });
    this.setState({ cansacox: false });
    this.setState({ chiadonopeitox: false });
    this.setState({coceirax: false });
    this.setState({ colicasx: false });
    this.setState({ diarreiax: false });
    this.setState({ dorabdominalx: false });
    this.setState({ dordecabecax: false });
    this.setState({ dorfacialx: false });
     this.setState({ dordeouvidox: false });
     this.setState({ doraoengolirx: false });
     this.setState({ dordegargantax: false });
     this.setState({ dormuscularx: false });
     this.setState({ dornopeitox: false });
     this.setState({ fadigax: false });
     this.setState({ faltadearfrequentex: false });
     this.setState({ faltadeapetitex: false });
     this.setState({ fraquezax: false });
     this.setState({ febrex: false });
     this.setState({ fotofobiax: false });
     this.setState({ fonofobiax: false });
     this.setState({ gasesx: false });
     this.setState({ gargantasecax: false });
     this.setState({ gargantainchadax: false });
     this.setState({ indisposicaox: false });
     this.setState({ irritabilidadex: false });
     this.setState({ insegurancax: false });
     this.setState({ malestarx: false });
     this.setState({ mauhalitox: false });
     this.setState({ manchasvermelhasx: false });
     this.setState({ nauseax: false });
     this.setState({ obstrucaonasalx: false });
     this.setState({ palidezx: false });
     this.setState({ perdadepesox: false });
     this.setState({ peleamareladax: false });
     this.setState({ sangramentonasalx: false });
     this.setState({ sonolenciax: false });
     this.setState({ sonoirregularx: false });
     this.setState({ sedeemexcessox: false });
     this.setState({ tonturax: false });
     this.setState({ tossesecax: false });
     this.setState({ ulcerasx: false });
     this.setState({ urinaescurax: false });
     this.setState({ urinaemexcesso: false });
     this.setState({ vistaembacadax: false });
     this.setState({ vomitosx: false });

                    }

              else if( (doraoengolirx==true) && (gargantasecax==true)|| (doraoengolirx==true) && 
                      (dordegargantax==true)||(doraoengolirx==true) && (dordeouvidox==true)||(doraoengolirx==true)
                       && (febrex==true) ||(dordegargantax==true) && (febrex==true)|| (gargantasecax==true) && (dordeouvidox==true)
                      || (dordegargantax==true) && (dordeouvidox==true)
                       || (dordeouvidox==true) && (febrex==true)) {
                       this.setState({ visible8: true });//faringite pequena
                    this.setState({ apatiax: false });
    this.setState({ ardorlocalizadox: false });
    this.setState({ angustiax: false });
    this.setState({ ansiedadex: false });
    this.setState({ apetiteemexcessox: false });
    this.setState({ bolhaslocalizadasx: false });
    this.setState({ cansacox: false });
    this.setState({ chiadonopeitox: false });
    this.setState({coceirax: false });
    this.setState({ colicasx: false });
    this.setState({ diarreiax: false });
    this.setState({ dorabdominalx: false });
    this.setState({ dordecabecax: false });
    this.setState({ dorfacialx: false });
     this.setState({ dordeouvidox: false });
     this.setState({ doraoengolirx: false });
     this.setState({ dordegargantax: false });
     this.setState({ dormuscularx: false });
     this.setState({ dornopeitox: false });
     this.setState({ fadigax: false });
     this.setState({ faltadearfrequentex: false });
     this.setState({ faltadeapetitex: false });
     this.setState({ fraquezax: false });
     this.setState({ febrex: false });
     this.setState({ fotofobiax: false });
     this.setState({ fonofobiax: false });
     this.setState({ gasesx: false });
     this.setState({ gargantasecax: false });
     this.setState({ gargantainchadax: false });
     this.setState({ indisposicaox: false });
     this.setState({ irritabilidadex: false });
     this.setState({ insegurancax: false });
     this.setState({ malestarx: false });
     this.setState({ mauhalitox: false });
     this.setState({ manchasvermelhasx: false });
     this.setState({ nauseax: false });
     this.setState({ obstrucaonasalx: false });
     this.setState({ palidezx: false });
     this.setState({ perdadepesox: false });
     this.setState({ peleamareladax: false });
     this.setState({ sangramentonasalx: false });
     this.setState({ sonolenciax: false });
     this.setState({ sonoirregularx: false });
     this.setState({ sedeemexcessox: false });
     this.setState({ tonturax: false });
     this.setState({ tossesecax: false });
     this.setState({ ulcerasx: false });
     this.setState({ urinaescurax: false });
     this.setState({ urinaemexcesso: false });
     this.setState({ vistaembacadax: false });
     this.setState({ vomitosx: false });

                    }

      }

         if( (colicasx==true) || (vomitosx==true)|| 
                  (gasesx==true)|| (diarreiax==true))
                  {

                    if( (colicasx==true) && (vomitosx==true) && (gasesx==true)||
                    (colicasx==true) && (vomitosx==true) && (diarreiax==true)||
                    (vomitosx==true) && (gasesx==true)&&(diarreiax==true) ||
                    (gasesx==true) && (colicasx==true) && (diarreiax==true)){
                  this.setState({ visible9: true });//infeccao intestinal media
                this.setState({ apatiax: false });
    this.setState({ ardorlocalizadox: false });
    this.setState({ angustiax: false });
    this.setState({ ansiedadex: false });
    this.setState({ apetiteemexcessox: false });
    this.setState({ bolhaslocalizadasx: false });
    this.setState({ cansacox: false });
    this.setState({ chiadonopeitox: false });
    this.setState({coceirax: false });
    this.setState({ colicasx: false });
    this.setState({ diarreiax: false });
    this.setState({ dorabdominalx: false });
    this.setState({ dordecabecax: false });
    this.setState({ dorfacialx: false });
     this.setState({ dordeouvidox: false });
     this.setState({ doraoengolirx: false });
     this.setState({ dordegargantax: false });
     this.setState({ dormuscularx: false });
     this.setState({ dornopeitox: false });
     this.setState({ fadigax: false });
     this.setState({ faltadearfrequentex: false });
     this.setState({ faltadeapetitex: false });
     this.setState({ fraquezax: false });
     this.setState({ febrex: false });
     this.setState({ fotofobiax: false });
     this.setState({ fonofobiax: false });
     this.setState({ gasesx: false });
     this.setState({ gargantasecax: false });
     this.setState({ gargantainchadax: false });
     this.setState({ indisposicaox: false });
     this.setState({ irritabilidadex: false });
     this.setState({ insegurancax: false });
     this.setState({ malestarx: false });
     this.setState({ mauhalitox: false });
     this.setState({ manchasvermelhasx: false });
     this.setState({ nauseax: false });
     this.setState({ obstrucaonasalx: false });
     this.setState({ palidezx: false });
     this.setState({ perdadepesox: false });
     this.setState({ peleamareladax: false });
     this.setState({ sangramentonasalx: false });
     this.setState({ sonolenciax: false });
     this.setState({ sonoirregularx: false });
     this.setState({ sedeemexcessox: false });
     this.setState({ tonturax: false });
     this.setState({ tossesecax: false });
     this.setState({ ulcerasx: false });
     this.setState({ urinaescurax: false });
     this.setState({ urinaemexcesso: false });
     this.setState({ vistaembacadax: false });
     this.setState({ vomitosx: false });

                    }

         }


          else  if( (irritabilidadex==true) || (nauseax==true)|| 
                  (fonofobiax==true)|| (dordecabecax==true)||
                  (fotofobiax==true)){
                    

                   if( (irritabilidadex==true) && (nauseax==true) && (fonofobiax==true)||
                          (irritabilidadex==true) && (nauseax==true) && (dordecabecax==true)||
                          (irritabilidadex==true) && (nauseax==true)&&(fotofobiax==true) ||
                          (nauseax==true) && (fonofobiax==true) && (dordecabecax==true) ||
                          (gargantasecax==true) && (dordegargantax==true) && (febrex==true) ||
                          (nauseax==true) && (fotofobiax==true) && (fonofobiax==true) ||
                          (nauseax==true) && (dordecabecax==true) && (fotofobiax==true) ||
                          (fonofobiax==true) && (irritabilidadex==true) && (dordecabecax==true) ||
                          (fonofobiax==true) && (irritabilidadex==true) && (fotofobiax==true) ||
                          (dordecabecax==true) && (irritabilidadex=true) && (fotofobiax==true)){
                        this.setState({ visible10: true });//infeccao enxaqueca grande
                      this.setState({ apatiax: false });
    this.setState({ ardorlocalizadox: false });
    this.setState({ angustiax: false });
    this.setState({ ansiedadex: false });
    this.setState({ apetiteemexcessox: false });
    this.setState({ bolhaslocalizadasx: false });
    this.setState({ cansacox: false });
    this.setState({ chiadonopeitox: false });
    this.setState({coceirax: false });
    this.setState({ colicasx: false });
    this.setState({ diarreiax: false });
    this.setState({ dorabdominalx: false });
    this.setState({ dordecabecax: false });
    this.setState({ dorfacialx: false });
     this.setState({ dordeouvidox: false });
     this.setState({ doraoengolirx: false });
     this.setState({ dordegargantax: false });
     this.setState({ dormuscularx: false });
     this.setState({ dornopeitox: false });
     this.setState({ fadigax: false });
     this.setState({ faltadearfrequentex: false });
     this.setState({ faltadeapetitex: false });
     this.setState({ fraquezax: false });
     this.setState({ febrex: false });
     this.setState({ fotofobiax: false });
     this.setState({ fonofobiax: false });
     this.setState({ gasesx: false });
     this.setState({ gargantasecax: false });
     this.setState({ gargantainchadax: false });
     this.setState({ indisposicaox: false });
     this.setState({ irritabilidadex: false });
     this.setState({ insegurancax: false });
     this.setState({ malestarx: false });
     this.setState({ mauhalitox: false });
     this.setState({ manchasvermelhasx: false });
     this.setState({ nauseax: false });
     this.setState({ obstrucaonasalx: false });
     this.setState({ palidezx: false });
     this.setState({ perdadepesox: false });
     this.setState({ peleamareladax: false });
     this.setState({ sangramentonasalx: false });
     this.setState({ sonolenciax: false });
     this.setState({ sonoirregularx: false });
     this.setState({ sedeemexcessox: false });
     this.setState({ tonturax: false });
     this.setState({ tossesecax: false });
     this.setState({ ulcerasx: false });
     this.setState({ urinaescurax: false });
     this.setState({ urinaemexcesso: false });
     this.setState({ vistaembacadax: false });
     this.setState({ vomitosx: false });

                          }

                    else if( (irritabilidadex==true) && (nauseax==true)|| (irritabilidadex==true) && 
                            (fonofobiax==true)||(irritabilidadex==true) && (dordecabecax==true)||(irritabilidadex==true)
                             && (fotofobiax==true) ||(fonofobiax==true) && (nauseax==true)|| (fonofobiax==true) && (dordecabecax==true)
                            || (fonofobiax==true) && (fotofobiax==true)
                             || (dordecabecax==true) && (fotofobiax==true)) {
                            this.setState({ visible11: true });// enxaqueca pequena
                          this.setState({ apatiax: false });
    this.setState({ ardorlocalizadox: false });
    this.setState({ angustiax: false });
    this.setState({ ansiedadex: false });
    this.setState({ apetiteemexcessox: false });
    this.setState({ bolhaslocalizadasx: false });
    this.setState({ cansacox: false });
    this.setState({ chiadonopeitox: false });
    this.setState({coceirax: false });
    this.setState({ colicasx: false });
    this.setState({ diarreiax: false });
    this.setState({ dorabdominalx: false });
    this.setState({ dordecabecax: false });
    this.setState({ dorfacialx: false });
     this.setState({ dordeouvidox: false });
     this.setState({ doraoengolirx: false });
     this.setState({ dordegargantax: false });
     this.setState({ dormuscularx: false });
     this.setState({ dornopeitox: false });
     this.setState({ fadigax: false });
     this.setState({ faltadearfrequentex: false });
     this.setState({ faltadeapetitex: false });
     this.setState({ fraquezax: false });
     this.setState({ febrex: false });
     this.setState({ fotofobiax: false });
     this.setState({ fonofobiax: false });
     this.setState({ gasesx: false });
     this.setState({ gargantasecax: false });
     this.setState({ gargantainchadax: false });
     this.setState({ indisposicaox: false });
     this.setState({ irritabilidadex: false });
     this.setState({ insegurancax: false });
     this.setState({ malestarx: false });
     this.setState({ mauhalitox: false });
     this.setState({ manchasvermelhasx: false });
     this.setState({ nauseax: false });
     this.setState({ obstrucaonasalx: false });
     this.setState({ palidezx: false });
     this.setState({ perdadepesox: false });
     this.setState({ peleamareladax: false });
     this.setState({ sangramentonasalx: false });
     this.setState({ sonolenciax: false });
     this.setState({ sonoirregularx: false });
     this.setState({ sedeemexcessox: false });
     this.setState({ tonturax: false });
     this.setState({ tossesecax: false });
     this.setState({ ulcerasx: false });
     this.setState({ urinaescurax: false });
     this.setState({ urinaemexcesso: false });
     this.setState({ vistaembacadax: false });
     this.setState({ vomitosx: false });

                          }

            }

         
             if( (tossesecax==true) || (faltadearfrequentex==true)|| 
                  (chiadonopeitox==true)|| (dornopeitox==true)){

                    if( (tossesecax==true) && (faltadearfrequentex==true) && (chiadonopeitox==true)||
                    (tossesecax==true) && (faltadearfrequentex==true) && (dornopeitox==true)||
                    (faltadearfrequentex==true) && (chiadonopeitox==true)&&(dornopeitox==true) ||
                    (chiadonopeitox==true) && (tossesecax==true) && (dornopeitox==true)){
                  this.setState({ visible12: true });//asma media
                this.setState({ apatiax: false });
    this.setState({ ardorlocalizadox: false });
    this.setState({ angustiax: false });
    this.setState({ ansiedadex: false });
    this.setState({ apetiteemexcessox: false });
    this.setState({ bolhaslocalizadasx: false });
    this.setState({ cansacox: false });
    this.setState({ chiadonopeitox: false });
    this.setState({coceirax: false });
    this.setState({ colicasx: false });
    this.setState({ diarreiax: false });
    this.setState({ dorabdominalx: false });
    this.setState({ dordecabecax: false });
    this.setState({ dorfacialx: false });
     this.setState({ dordeouvidox: false });
     this.setState({ doraoengolirx: false });
     this.setState({ dordegargantax: false });
     this.setState({ dormuscularx: false });
     this.setState({ dornopeitox: false });
     this.setState({ fadigax: false });
     this.setState({ faltadearfrequentex: false });
     this.setState({ faltadeapetitex: false });
     this.setState({ fraquezax: false });
     this.setState({ febrex: false });
     this.setState({ fotofobiax: false });
     this.setState({ fonofobiax: false });
     this.setState({ gasesx: false });
     this.setState({ gargantasecax: false });
     this.setState({ gargantainchadax: false });
     this.setState({ indisposicaox: false });
     this.setState({ irritabilidadex: false });
     this.setState({ insegurancax: false });
     this.setState({ malestarx: false });
     this.setState({ mauhalitox: false });
     this.setState({ manchasvermelhasx: false });
     this.setState({ nauseax: false });
     this.setState({ obstrucaonasalx: false });
     this.setState({ palidezx: false });
     this.setState({ perdadepesox: false });
     this.setState({ peleamareladax: false });
     this.setState({ sangramentonasalx: false });
     this.setState({ sonolenciax: false });
     this.setState({ sonoirregularx: false });
     this.setState({ sedeemexcessox: false });
     this.setState({ tonturax: false });
     this.setState({ tossesecax: false });
     this.setState({ ulcerasx: false });
     this.setState({ urinaescurax: false });
     this.setState({ urinaemexcesso: false });
     this.setState({ vistaembacadax: false });
     this.setState({ vomitosx: false });

                    }

             }
         
         if( (apatiax==true) || (insegurancax==true)|| 
          (sonoirregularx==true)|| (ansiedadex==true)||
          (angustiax==true) ){

              if( (apatiax==true) && (insegurancax==true) && (sonoirregularx==true)||
                (apatiax==true) && (insegurancax==true) && (ansiedadex==true)||
                (apatiax==true) && (insegurancax==true)&&(ansiedadex==true) ||
                (insegurancax==true) && (sonoirregularx==true) && (ansiedadex==true) ||
                (insegurancax==true) && (sonoirregularx==true) && (angustiax==true) ||
                (insegurancax==true) && (ansiedadex==true) && (angustiax==true) ||
                (sonoirregularx==true) && (apatiax==true) && (ansiedadex==true) ||
                (sonoirregularx==true) && (apatiax==true) && (angustiax==true) ||
                (sonoirregularx==true) && (ansiedadex==true) && (angustiax==true) ||
                (ansiedadex==true) && (apatiax==true) && (angustiax==true)){
              this.setState({ visible13: true });//depressao media
            this.setState({ apatiax: false });
    this.setState({ ardorlocalizadox: false });
    this.setState({ angustiax: false });
    this.setState({ ansiedadex: false });
    this.setState({ apetiteemexcessox: false });
    this.setState({ bolhaslocalizadasx: false });
    this.setState({ cansacox: false });
    this.setState({ chiadonopeitox: false });
    this.setState({coceirax: false });
    this.setState({ colicasx: false });
    this.setState({ diarreiax: false });
    this.setState({ dorabdominalx: false });
    this.setState({ dordecabecax: false });
    this.setState({ dorfacialx: false });
     this.setState({ dordeouvidox: false });
     this.setState({ doraoengolirx: false });
     this.setState({ dordegargantax: false });
     this.setState({ dormuscularx: false });
     this.setState({ dornopeitox: false });
     this.setState({ fadigax: false });
     this.setState({ faltadearfrequentex: false });
     this.setState({ faltadeapetitex: false });
     this.setState({ fraquezax: false });
     this.setState({ febrex: false });
     this.setState({ fotofobiax: false });
     this.setState({ fonofobiax: false });
     this.setState({ gasesx: false });
     this.setState({ gargantasecax: false });
     this.setState({ gargantainchadax: false });
     this.setState({ indisposicaox: false });
     this.setState({ irritabilidadex: false });
     this.setState({ insegurancax: false });
     this.setState({ malestarx: false });
     this.setState({ mauhalitox: false });
     this.setState({ manchasvermelhasx: false });
     this.setState({ nauseax: false });
     this.setState({ obstrucaonasalx: false });
     this.setState({ palidezx: false });
     this.setState({ perdadepesox: false });
     this.setState({ peleamareladax: false });
     this.setState({ sangramentonasalx: false });
     this.setState({ sonolenciax: false });
     this.setState({ sonoirregularx: false });
     this.setState({ sedeemexcessox: false });
     this.setState({ tonturax: false });
     this.setState({ tossesecax: false });
     this.setState({ ulcerasx: false });
     this.setState({ urinaescurax: false });
     this.setState({ urinaemexcesso: false });
     this.setState({ vistaembacadax: false });
     this.setState({ vomitosx: false });

                }

                else if( (apatiax==true) && (insegurancax==true)|| (apatiax==true) && 
                (sonoirregularx==true)||(apatiax==true) && (ansiedadex==true)||(apatiax==true)
                 && (angustiax==true) || (sonoirregularx==true) && (insegurancax==true) 
                 || (sonoirregularx==true) && (ansiedadex==true)|| (sonoirregularx==true) && (angustiax==true)
                 || (insegurancax==true) && (ansiedadex==true)|| (insegurancax==true) && (angustiax==true)
                 || (ansiedadex==true) && (angustiax==true)) {
                 this.setState({ visible14: true });//depressao pequena
              this.setState({ apatiax: false });
    this.setState({ ardorlocalizadox: false });
    this.setState({ angustiax: false });
    this.setState({ ansiedadex: false });
    this.setState({ apetiteemexcessox: false });
    this.setState({ bolhaslocalizadasx: false });
    this.setState({ cansacox: false });
    this.setState({ chiadonopeitox: false });
    this.setState({coceirax: false });
    this.setState({ colicasx: false });
    this.setState({ diarreiax: false });
    this.setState({ dorabdominalx: false });
    this.setState({ dordecabecax: false });
    this.setState({ dorfacialx: false });
     this.setState({ dordeouvidox: false });
     this.setState({ doraoengolirx: false });
     this.setState({ dordegargantax: false });
     this.setState({ dormuscularx: false });
     this.setState({ dornopeitox: false });
     this.setState({ fadigax: false });
     this.setState({ faltadearfrequentex: false });
     this.setState({ faltadeapetitex: false });
     this.setState({ fraquezax: false });
     this.setState({ febrex: false });
     this.setState({ fotofobiax: false });
     this.setState({ fonofobiax: false });
     this.setState({ gasesx: false });
     this.setState({ gargantasecax: false });
     this.setState({ gargantainchadax: false });
     this.setState({ indisposicaox: false });
     this.setState({ irritabilidadex: false });
     this.setState({ insegurancax: false });
     this.setState({ malestarx: false });
     this.setState({ mauhalitox: false });
     this.setState({ manchasvermelhasx: false });
     this.setState({ nauseax: false });
     this.setState({ obstrucaonasalx: false });
     this.setState({ palidezx: false });
     this.setState({ perdadepesox: false });
     this.setState({ peleamareladax: false });
     this.setState({ sangramentonasalx: false });
     this.setState({ sonolenciax: false });
     this.setState({ sonoirregularx: false });
     this.setState({ sedeemexcessox: false });
     this.setState({ tonturax: false });
     this.setState({ tossesecax: false });
     this.setState({ ulcerasx: false });
     this.setState({ urinaescurax: false });
     this.setState({ urinaemexcesso: false });
     this.setState({ vistaembacadax: false });
     this.setState({ vomitosx: false });

              }
          }

        if( (urinaemexcessox==true) || (sedeemexcessox==true)|| 
          (vistaembacadax==true)|| (perdadepesox==true)||
          (apetiteemexcessox==true) ){

              if( (urinaemexcessox==true) && (sedeemexcessox==true) && (vistaembacadax==true)||
                (urinaemexcessox==true) && (sedeemexcessox==true) && (perdadepesox==true)||
                (urinaemexcessox==true) && (sedeemexcessox==true)&&(apetiteemexcessox==true) ||
                (sedeemexcessox==true) && (vistaembacadax==true) && (perdadepesox==true) ||
                (sedeemexcessox==true) && (vistaembacadax==true) && (apetiteemexcessox==true) ||
                (sedeemexcessox==true) && (perdadepesox==true) && (apetiteemexcessox==true) ||
                (vistaembacadax==true) && (urinaemexcessox==true) && (perdadepesox==true) ||
                (vistaembacadax==true) && (urinaemexcessox==true) && (apetiteemexcessox==true) ||
                (vistaembacadax==true) && (perdadepesox==true) && (apetiteemexcessox==true) ||
                (perdadepesox==true) && (urinaemexcessox==true) && (apetiteemexcessox==true)){
              this.setState({ visible15: true });//diabetes pequena
            this.setState({ apatiax: false });
    this.setState({ ardorlocalizadox: false });
    this.setState({ angustiax: false });
    this.setState({ ansiedadex: false });
    this.setState({ apetiteemexcessox: false });
    this.setState({ bolhaslocalizadasx: false });
    this.setState({ cansacox: false });
    this.setState({ chiadonopeitox: false });
    this.setState({coceirax: false });
    this.setState({ colicasx: false });
    this.setState({ diarreiax: false });
    this.setState({ dorabdominalx: false });
    this.setState({ dordecabecax: false });
    this.setState({ dorfacialx: false });
     this.setState({ dordeouvidox: false });
     this.setState({ doraoengolirx: false });
     this.setState({ dordegargantax: false });
     this.setState({ dormuscularx: false });
     this.setState({ dornopeitox: false });
     this.setState({ fadigax: false });
     this.setState({ faltadearfrequentex: false });
     this.setState({ faltadeapetitex: false });
     this.setState({ fraquezax: false });
     this.setState({ febrex: false });
     this.setState({ fotofobiax: false });
     this.setState({ fonofobiax: false });
     this.setState({ gasesx: false });
     this.setState({ gargantasecax: false });
     this.setState({ gargantainchadax: false });
     this.setState({ indisposicaox: false });
     this.setState({ irritabilidadex: false });
     this.setState({ insegurancax: false });
     this.setState({ malestarx: false });
     this.setState({ mauhalitox: false });
     this.setState({ manchasvermelhasx: false });
     this.setState({ nauseax: false });
     this.setState({ obstrucaonasalx: false });
     this.setState({ palidezx: false });
     this.setState({ perdadepesox: false });
     this.setState({ peleamareladax: false });
     this.setState({ sangramentonasalx: false });
     this.setState({ sonolenciax: false });
     this.setState({ sonoirregularx: false });
     this.setState({ sedeemexcessox: false });
     this.setState({ tonturax: false });
     this.setState({ tossesecax: false });
     this.setState({ ulcerasx: false });
     this.setState({ urinaescurax: false });
     this.setState({ urinaemexcesso: false });
     this.setState({ vistaembacadax: false });
     this.setState({ vomitosx: false });

                }
          } 

       else  if( (dornopeitox==true) || (tonturax==true)|| 
          (sangramentonasalx==true)|| (fraquezax==true)||
          (dordecabecax==true) ){

              if( (dornopeitox==true) && (tonturax==true) && (sangramentonasalx==true)||
                (dornopeitox==true) && (tonturax==true) && (fraquezax==true)||
                (dornopeitox==true) && (tonturax==true)&&(dordecabecax==true) ||
                (tonturax==true) && (sangramentonasalx==true) && (fraquezax==true) ||
                (tonturax==true) && (sangramentonasalx==true) && (dordecabecax==true) ||
                (tonturax==true) && (fraquezax==true) && (dordecabecax==true) ||
                (sangramentonasalx==true) && (dornopeitox==true) && (fraquezax==true) ||
                (sangramentonasalx==true) && (dornopeitox==true) && (dordecabecax==true) ||
                (sangramentonasalx==true) && (fraquezax==true) && (dordecabecax==true) ||
                (fraquezax==true) && (dornopeitox==true) && (dordecabecax==true)){
              this.setState({ visible16: true });//pressão alta media
            this.setState({ apatiax: false });
    this.setState({ ardorlocalizadox: false });
    this.setState({ angustiax: false });
    this.setState({ ansiedadex: false });
    this.setState({ apetiteemexcessox: false });
    this.setState({ bolhaslocalizadasx: false });
    this.setState({ cansacox: false });
    this.setState({ chiadonopeitox: false });
    this.setState({coceirax: false });
    this.setState({ colicasx: false });
    this.setState({ diarreiax: false });
    this.setState({ dorabdominalx: false });
    this.setState({ dordecabecax: false });
    this.setState({ dorfacialx: false });
     this.setState({ dordeouvidox: false });
     this.setState({ doraoengolirx: false });
     this.setState({ dordegargantax: false });
     this.setState({ dormuscularx: false });
     this.setState({ dornopeitox: false });
     this.setState({ fadigax: false });
     this.setState({ faltadearfrequentex: false });
     this.setState({ faltadeapetitex: false });
     this.setState({ fraquezax: false });
     this.setState({ febrex: false });
     this.setState({ fotofobiax: false });
     this.setState({ fonofobiax: false });
     this.setState({ gasesx: false });
     this.setState({ gargantasecax: false });
     this.setState({ gargantainchadax: false });
     this.setState({ indisposicaox: false });
     this.setState({ irritabilidadex: false });
     this.setState({ insegurancax: false });
     this.setState({ malestarx: false });
     this.setState({ mauhalitox: false });
     this.setState({ manchasvermelhasx: false });
     this.setState({ nauseax: false });
     this.setState({ obstrucaonasalx: false });
     this.setState({ palidezx: false });
     this.setState({ perdadepesox: false });
     this.setState({ peleamareladax: false });
     this.setState({ sangramentonasalx: false });
     this.setState({ sonolenciax: false });
     this.setState({ sonoirregularx: false });
     this.setState({ sedeemexcessox: false });
     this.setState({ tonturax: false });
     this.setState({ tossesecax: false });
     this.setState({ ulcerasx: false });
     this.setState({ urinaescurax: false });
     this.setState({ urinaemexcesso: false });
     this.setState({ vistaembacadax: false });
     this.setState({ vomitosx: false });

                }

                else if( (dornopeitox==true) && (tonturax==true)|| (dornopeitox==true) && 
                (sangramentonasalx==true)||(dornopeitox==true) && (fraquezax==true)||(dornopeitox==true)
                 && (dordecabecax==true) || (sangramentonasalx==true) && (tonturax==true) 
                 || (sangramentonasalx==true) && (fraquezax==true)|| (sangramentonasalx==true) && (dordecabecax==true)
                 || (tonturax==true) && (fraquezax==true)|| (tonturax==true) && (dordecabecax==true)
                 || (fraquezax==true) && (dordecabecax==true)) {
                this.setState({ visible17: true });//pressão alta pequena
              this.setState({ apatiax: false });
    this.setState({ ardorlocalizadox: false });
    this.setState({ angustiax: false });
    this.setState({ ansiedadex: false });
    this.setState({ apetiteemexcessox: false });
    this.setState({ bolhaslocalizadasx: false });
    this.setState({ cansacox: false });
    this.setState({ chiadonopeitox: false });
    this.setState({coceirax: false });
    this.setState({ colicasx: false });
    this.setState({ diarreiax: false });
    this.setState({ dorabdominalx: false });
    this.setState({ dordecabecax: false });
    this.setState({ dorfacialx: false });
     this.setState({ dordeouvidox: false });
     this.setState({ doraoengolirx: false });
     this.setState({ dordegargantax: false });
     this.setState({ dormuscularx: false });
     this.setState({ dornopeitox: false });
     this.setState({ fadigax: false });
     this.setState({ faltadearfrequentex: false });
     this.setState({ faltadeapetitex: false });
     this.setState({ fraquezax: false });
     this.setState({ febrex: false });
     this.setState({ fotofobiax: false });
     this.setState({ fonofobiax: false });
     this.setState({ gasesx: false });
     this.setState({ gargantasecax: false });
     this.setState({ gargantainchadax: false });
     this.setState({ indisposicaox: false });
     this.setState({ irritabilidadex: false });
     this.setState({ insegurancax: false });
     this.setState({ malestarx: false });
     this.setState({ mauhalitox: false });
     this.setState({ manchasvermelhasx: false });
     this.setState({ nauseax: false });
     this.setState({ obstrucaonasalx: false });
     this.setState({ palidezx: false });
     this.setState({ perdadepesox: false });
     this.setState({ peleamareladax: false });
     this.setState({ sangramentonasalx: false });
     this.setState({ sonolenciax: false });
     this.setState({ sonoirregularx: false });
     this.setState({ sedeemexcessox: false });
     this.setState({ tonturax: false });
     this.setState({ tossesecax: false });
     this.setState({ ulcerasx: false });
     this.setState({ urinaescurax: false });
     this.setState({ urinaemexcesso: false });
     this.setState({ vistaembacadax: false });
     this.setState({ vomitosx: false });

              }
          }

            if( (manchasvermelhasx==true) || (dormuscularx==true)|| 
          (faltadeapetitex==true)|| (febrex==true)||
          (vomitosx==true) ){

              if( (manchasvermelhasx==true) && (dormuscularx==true) && (faltadeapetitex==true)||
                (manchasvermelhasx==true) && (dormuscularx==true) && (febrex==true)||
                (manchasvermelhasx==true) && (dormuscularx==true)&&(vomitosx==true) ||
                (dormuscularx==true) && (faltadeapetitex==true) && (febrex==true) ||
                (dormuscularx==true) && (faltadeapetitex==true) && (vomitosx==true) ||
                (dormuscularx==true) && (febrex==true) && (vomitosx==true) ||
                (faltadeapetitex==true) && (manchasvermelhasx==true) && (febrex==true) ||
                (faltadeapetitex==true) && (manchasvermelhasx==true) && (vomitosx==true) ||
                (faltadeapetitex==true) && (febrex==true) && (vomitosx==true) ||
                (febrex==true) && (manchasvermelhasx==true) && (vomitosx==true)){

                if( (manchasvermelhasx==true) && (dormuscularx==true) && (faltadeapetitex==true)||
                (manchasvermelhasx==true) && (dormuscularx==true) && (febrex==true)||
                (manchasvermelhasx==true) && (dormuscularx==true)&&(vomitosx==true) ||
                (faltadeapetitex==true) && (manchasvermelhasx==true) && (febrex==true) ||
                (faltadeapetitex==true) && (manchasvermelhasx==true) && (vomitosx==true) ||
                (febrex==true) && (manchasvermelhasx==true) && (vomitosx==true)){
                  this.setState({ visible18: true });//pressão dengue grande
                this.setState({ apatiax: false });
    this.setState({ ardorlocalizadox: false });
    this.setState({ angustiax: false });
    this.setState({ ansiedadex: false });
    this.setState({ apetiteemexcessox: false });
    this.setState({ bolhaslocalizadasx: false });
    this.setState({ cansacox: false });
    this.setState({ chiadonopeitox: false });
    this.setState({coceirax: false });
    this.setState({ colicasx: false });
    this.setState({ diarreiax: false });
    this.setState({ dorabdominalx: false });
    this.setState({ dordecabecax: false });
    this.setState({ dorfacialx: false });
     this.setState({ dordeouvidox: false });
     this.setState({ doraoengolirx: false });
     this.setState({ dordegargantax: false });
     this.setState({ dormuscularx: false });
     this.setState({ dornopeitox: false });
     this.setState({ fadigax: false });
     this.setState({ faltadearfrequentex: false });
     this.setState({ faltadeapetitex: false });
     this.setState({ fraquezax: false });
     this.setState({ febrex: false });
     this.setState({ fotofobiax: false });
     this.setState({ fonofobiax: false });
     this.setState({ gasesx: false });
     this.setState({ gargantasecax: false });
     this.setState({ gargantainchadax: false });
     this.setState({ indisposicaox: false });
     this.setState({ irritabilidadex: false });
     this.setState({ insegurancax: false });
     this.setState({ malestarx: false });
     this.setState({ mauhalitox: false });
     this.setState({ manchasvermelhasx: false });
     this.setState({ nauseax: false });
     this.setState({ obstrucaonasalx: false });
     this.setState({ palidezx: false });
     this.setState({ perdadepesox: false });
     this.setState({ peleamareladax: false });
     this.setState({ sangramentonasalx: false });
     this.setState({ sonolenciax: false });
     this.setState({ sonoirregularx: false });
     this.setState({ sedeemexcessox: false });
     this.setState({ tonturax: false });
     this.setState({ tossesecax: false });
     this.setState({ ulcerasx: false });
     this.setState({ urinaescurax: false });
     this.setState({ urinaemexcesso: false });
     this.setState({ vistaembacadax: false });
     this.setState({ vomitosx: false });


                }
              
                         else{
                          this.setState({ visible19: true });// dengue media
                          this.setState({ apatiax: false });
    this.setState({ ardorlocalizadox: false });
    this.setState({ angustiax: false });
    this.setState({ ansiedadex: false });
    this.setState({ apetiteemexcessox: false });
    this.setState({ bolhaslocalizadasx: false });
    this.setState({ cansacox: false });
    this.setState({ chiadonopeitox: false });
    this.setState({coceirax: false });
    this.setState({ colicasx: false });
    this.setState({ diarreiax: false });
    this.setState({ dorabdominalx: false });
    this.setState({ dordecabecax: false });
    this.setState({ dorfacialx: false });
     this.setState({ dordeouvidox: false });
     this.setState({ doraoengolirx: false });
     this.setState({ dordegargantax: false });
     this.setState({ dormuscularx: false });
     this.setState({ dornopeitox: false });
     this.setState({ fadigax: false });
     this.setState({ faltadearfrequentex: false });
     this.setState({ faltadeapetitex: false });
     this.setState({ fraquezax: false });
     this.setState({ febrex: false });
     this.setState({ fotofobiax: false });
     this.setState({ fonofobiax: false });
     this.setState({ gasesx: false });
     this.setState({ gargantasecax: false });
     this.setState({ gargantainchadax: false });
     this.setState({ indisposicaox: false });
     this.setState({ irritabilidadex: false });
     this.setState({ insegurancax: false });
     this.setState({ malestarx: false });
     this.setState({ mauhalitox: false });
     this.setState({ manchasvermelhasx: false });
     this.setState({ nauseax: false });
     this.setState({ obstrucaonasalx: false });
     this.setState({ palidezx: false });
     this.setState({ perdadepesox: false });
     this.setState({ peleamareladax: false });
     this.setState({ sangramentonasalx: false });
     this.setState({ sonolenciax: false });
     this.setState({ sonoirregularx: false });
     this.setState({ sedeemexcessox: false });
     this.setState({ tonturax: false });
     this.setState({ tossesecax: false });
     this.setState({ ulcerasx: false });
     this.setState({ urinaescurax: false });
     this.setState({ urinaemexcesso: false });
     this.setState({ vistaembacadax: false });
     this.setState({ vomitosx: false });

                         }

                }

                else if( (manchasvermelhasx==true) && (dormuscularx==true)|| (manchasvermelhasx==true) && 
                (faltadeapetitex==true)||(manchasvermelhasx==true) && (febrex==true)||(manchasvermelhasx==true)
                 && (vomitosx==true)) {
                this.setState({ visible20: true });// dengue pequena
              this.setState({ apatiax: false });
    this.setState({ ardorlocalizadox: false });
    this.setState({ angustiax: false });
    this.setState({ ansiedadex: false });
    this.setState({ apetiteemexcessox: false });
    this.setState({ bolhaslocalizadasx: false });
    this.setState({ cansacox: false });
    this.setState({ chiadonopeitox: false });
    this.setState({coceirax: false });
    this.setState({ colicasx: false });
    this.setState({ diarreiax: false });
    this.setState({ dorabdominalx: false });
    this.setState({ dordecabecax: false });
    this.setState({ dorfacialx: false });
     this.setState({ dordeouvidox: false });
     this.setState({ doraoengolirx: false });
     this.setState({ dordegargantax: false });
     this.setState({ dormuscularx: false });
     this.setState({ dornopeitox: false });
     this.setState({ fadigax: false });
     this.setState({ faltadearfrequentex: false });
     this.setState({ faltadeapetitex: false });
     this.setState({ fraquezax: false });
     this.setState({ febrex: false });
     this.setState({ fotofobiax: false });
     this.setState({ fonofobiax: false });
     this.setState({ gasesx: false });
     this.setState({ gargantasecax: false });
     this.setState({ gargantainchadax: false });
     this.setState({ indisposicaox: false });
     this.setState({ irritabilidadex: false });
     this.setState({ insegurancax: false });
     this.setState({ malestarx: false });
     this.setState({ mauhalitox: false });
     this.setState({ manchasvermelhasx: false });
     this.setState({ nauseax: false });
     this.setState({ obstrucaonasalx: false });
     this.setState({ palidezx: false });
     this.setState({ perdadepesox: false });
     this.setState({ peleamareladax: false });
     this.setState({ sangramentonasalx: false });
     this.setState({ sonolenciax: false });
     this.setState({ sonoirregularx: false });
     this.setState({ sedeemexcessox: false });
     this.setState({ tonturax: false });
     this.setState({ tossesecax: false });
     this.setState({ ulcerasx: false });
     this.setState({ urinaescurax: false });
     this.setState({ urinaemexcesso: false });
     this.setState({ vistaembacadax: false });
     this.setState({ vomitosx: false });

              }
          }
      
      if( (sonolenciax==true) || (irritabilidadex==true)|| 
                  (fadigax==true)|| (sonoirregularx==true))
                  {

                    if( (sonoirregularx==true) && (fadigax==true) ||
                    (sonoirregularx==true) && (sonolenciax==true)||
                    (sonoirregularx==true) && (irritabilidadex==true) ||
                    (sonolenciax==true) && (irritabilidadex==true) ||
                    (sonolenciax==true) && (fadigax==true) ){
                  this.setState({ visible21: true });// insonia grande
                this.setState({ apatiax: false });
    this.setState({ ardorlocalizadox: false });
    this.setState({ angustiax: false });
    this.setState({ ansiedadex: false });
    this.setState({ apetiteemexcessox: false });
    this.setState({ bolhaslocalizadasx: false });
    this.setState({ cansacox: false });
    this.setState({ chiadonopeitox: false });
    this.setState({coceirax: false });
    this.setState({ colicasx: false });
    this.setState({ diarreiax: false });
    this.setState({ dorabdominalx: false });
    this.setState({ dordecabecax: false });
    this.setState({ dorfacialx: false });
     this.setState({ dordeouvidox: false });
     this.setState({ doraoengolirx: false });
     this.setState({ dordegargantax: false });
     this.setState({ dormuscularx: false });
     this.setState({ dornopeitox: false });
     this.setState({ fadigax: false });
     this.setState({ faltadearfrequentex: false });
     this.setState({ faltadeapetitex: false });
     this.setState({ fraquezax: false });
     this.setState({ febrex: false });
     this.setState({ fotofobiax: false });
     this.setState({ fonofobiax: false });
     this.setState({ gasesx: false });
     this.setState({ gargantasecax: false });
     this.setState({ gargantainchadax: false });
     this.setState({ indisposicaox: false });
     this.setState({ irritabilidadex: false });
     this.setState({ insegurancax: false });
     this.setState({ malestarx: false });
     this.setState({ mauhalitox: false });
     this.setState({ manchasvermelhasx: false });
     this.setState({ nauseax: false });
     this.setState({ obstrucaonasalx: false });
     this.setState({ palidezx: false });
     this.setState({ perdadepesox: false });
     this.setState({ peleamareladax: false });
     this.setState({ sangramentonasalx: false });
     this.setState({ sonolenciax: false });
     this.setState({ sonoirregularx: false });
     this.setState({ sedeemexcessox: false });
     this.setState({ tonturax: false });
     this.setState({ tossesecax: false });
     this.setState({ ulcerasx: false });
     this.setState({ urinaescurax: false });
     this.setState({ urinaemexcesso: false });
     this.setState({ vistaembacadax: false });
     this.setState({ vomitosx: false });

                    }
         }
  
        else   if( (bolhaslocalizadasx==true) || (ulcerasx==true)|| 
                  (ardorlocalizadox==true)|| (coceirax==true))
                  {

                    if( (bolhaslocalizadasx==true) && (ulcerasx==true) && (ardorlocalizadox==true)||
                    (bolhaslocalizadasx==true) && (ulcerasx==true) && (coceirax==true)||
                    (ulcerasx==true) && (ardorlocalizadox==true)&&(coceirax==true) ||
                    (ardorlocalizadox==true) && (bolhaslocalizadasx==true) && (coceirax==true)){
                   this.setState({ visible22: true });// herpes grande
                this.setState({ apatiax: false });
    this.setState({ ardorlocalizadox: false });
    this.setState({ angustiax: false });
    this.setState({ ansiedadex: false });
    this.setState({ apetiteemexcessox: false });
    this.setState({ bolhaslocalizadasx: false });
    this.setState({ cansacox: false });
    this.setState({ chiadonopeitox: false });
    this.setState({coceirax: false });
    this.setState({ colicasx: false });
    this.setState({ diarreiax: false });
    this.setState({ dorabdominalx: false });
    this.setState({ dordecabecax: false });
    this.setState({ dorfacialx: false });
     this.setState({ dordeouvidox: false });
     this.setState({ doraoengolirx: false });
     this.setState({ dordegargantax: false });
     this.setState({ dormuscularx: false });
     this.setState({ dornopeitox: false });
     this.setState({ fadigax: false });
     this.setState({ faltadearfrequentex: false });
     this.setState({ faltadeapetitex: false });
     this.setState({ fraquezax: false });
     this.setState({ febrex: false });
     this.setState({ fotofobiax: false });
     this.setState({ fonofobiax: false });
     this.setState({ gasesx: false });
     this.setState({ gargantasecax: false });
     this.setState({ gargantainchadax: false });
     this.setState({ indisposicaox: false });
     this.setState({ irritabilidadex: false });
     this.setState({ insegurancax: false });
     this.setState({ malestarx: false });
     this.setState({ mauhalitox: false });
     this.setState({ manchasvermelhasx: false });
     this.setState({ nauseax: false });
     this.setState({ obstrucaonasalx: false });
     this.setState({ palidezx: false });
     this.setState({ perdadepesox: false });
     this.setState({ peleamareladax: false });
     this.setState({ sangramentonasalx: false });
     this.setState({ sonolenciax: false });
     this.setState({ sonoirregularx: false });
     this.setState({ sedeemexcessox: false });
     this.setState({ tonturax: false });
     this.setState({ tossesecax: false });
     this.setState({ ulcerasx: false });
     this.setState({ urinaescurax: false });
     this.setState({ urinaemexcesso: false });
     this.setState({ vistaembacadax: false });
     this.setState({ vomitosx: false });

                    }

         }
          
         else     if( (gargantainchadax==true) || (doraoengolirx==true)|| 
                  (fadigax==true)){


                 if( (gargantainchadax==true) && (doraoengolirx==true)|| (gargantainchadax==true) && 
                (fadigax==true)||(doraoengolirx==true) && (fadigax==true)) {
                this.setState({ visible23: true });// caxumba grande
              this.setState({ apatiax: false });
    this.setState({ ardorlocalizadox: false });
    this.setState({ angustiax: false });
    this.setState({ ansiedadex: false });
    this.setState({ apetiteemexcessox: false });
    this.setState({ bolhaslocalizadasx: false });
    this.setState({ cansacox: false });
    this.setState({ chiadonopeitox: false });
    this.setState({coceirax: false });
    this.setState({ colicasx: false });
    this.setState({ diarreiax: false });
    this.setState({ dorabdominalx: false });
    this.setState({ dordecabecax: false });
    this.setState({ dorfacialx: false });
     this.setState({ dordeouvidox: false });
     this.setState({ doraoengolirx: false });
     this.setState({ dordegargantax: false });
     this.setState({ dormuscularx: false });
     this.setState({ dornopeitox: false });
     this.setState({ fadigax: false });
     this.setState({ faltadearfrequentex: false });
     this.setState({ faltadeapetitex: false });
     this.setState({ fraquezax: false });
     this.setState({ febrex: false });
     this.setState({ fotofobiax: false });
     this.setState({ fonofobiax: false });
     this.setState({ gasesx: false });
     this.setState({ gargantasecax: false });
     this.setState({ gargantainchadax: false });
     this.setState({ indisposicaox: false });
     this.setState({ irritabilidadex: false });
     this.setState({ insegurancax: false });
     this.setState({ malestarx: false });
     this.setState({ mauhalitox: false });
     this.setState({ manchasvermelhasx: false });
     this.setState({ nauseax: false });
     this.setState({ obstrucaonasalx: false });
     this.setState({ palidezx: false });
     this.setState({ perdadepesox: false });
     this.setState({ peleamareladax: false });
     this.setState({ sangramentonasalx: false });
     this.setState({ sonolenciax: false });
     this.setState({ sonoirregularx: false });
     this.setState({ sedeemexcessox: false });
     this.setState({ tonturax: false });
     this.setState({ tossesecax: false });
     this.setState({ ulcerasx: false });
     this.setState({ urinaescurax: false });
     this.setState({ urinaemexcesso: false });
     this.setState({ vistaembacadax: false });
     this.setState({ vomitosx: false });

              }

              }

    else  if( (dorabdominalx==true) || (urinaescurax==true)|| 
          (vomitosx==true)|| (peleamareladax==true)||
          (malestarx==true) ){

              if( (dorabdominalx==true) && (urinaescurax==true) && (vomitosx==true)||
                (dorabdominalx==true) && (urinaescurax==true) && (peleamareladax==true)||
                (dorabdominalx==true) && (urinaescurax==true)&&(malestarx==true) ||
                (urinaescurax==true) && (vomitosx==true) && (peleamareladax==true) ||
                (urinaescurax==true) && (vomitosx==true) && (malestarx==true) ||
                (urinaescurax==true) && (peleamareladax==true) && (malestarx==true) ||
                (vomitosx==true) && (dorabdominalx==true) && (peleamareladax==true) ||
                (vomitosx==true) && (dorabdominalx==true) && (malestarx==true) ||
                (vomitosx==true) && (peleamareladax==true) && (malestarx==true) ||
                (peleamareladax==true) && (dorabdominalx==true) && (malestarx==true)){
              this.setState({ visible24: true });// hepatite c media
            this.setState({ apatiax: false });
    this.setState({ ardorlocalizadox: false });
    this.setState({ angustiax: false });
    this.setState({ ansiedadex: false });
    this.setState({ apetiteemexcessox: false });
    this.setState({ bolhaslocalizadasx: false });
    this.setState({ cansacox: false });
    this.setState({ chiadonopeitox: false });
    this.setState({coceirax: false });
    this.setState({ colicasx: false });
    this.setState({ diarreiax: false });
    this.setState({ dorabdominalx: false });
    this.setState({ dordecabecax: false });
    this.setState({ dorfacialx: false });
     this.setState({ dordeouvidox: false });
     this.setState({ doraoengolirx: false });
     this.setState({ dordegargantax: false });
     this.setState({ dormuscularx: false });
     this.setState({ dornopeitox: false });
     this.setState({ fadigax: false });
     this.setState({ faltadearfrequentex: false });
     this.setState({ faltadeapetitex: false });
     this.setState({ fraquezax: false });
     this.setState({ febrex: false });
     this.setState({ fotofobiax: false });
     this.setState({ fonofobiax: false });
     this.setState({ gasesx: false });
     this.setState({ gargantasecax: false });
     this.setState({ gargantainchadax: false });
     this.setState({ indisposicaox: false });
     this.setState({ irritabilidadex: false });
     this.setState({ insegurancax: false });
     this.setState({ malestarx: false });
     this.setState({ mauhalitox: false });
     this.setState({ manchasvermelhasx: false });
     this.setState({ nauseax: false });
     this.setState({ obstrucaonasalx: false });
     this.setState({ palidezx: false });
     this.setState({ perdadepesox: false });
     this.setState({ peleamareladax: false });
     this.setState({ sangramentonasalx: false });
     this.setState({ sonolenciax: false });
     this.setState({ sonoirregularx: false });
     this.setState({ sedeemexcessox: false });
     this.setState({ tonturax: false });
     this.setState({ tossesecax: false });
     this.setState({ ulcerasx: false });
     this.setState({ urinaescurax: false });
     this.setState({ urinaemexcesso: false });
     this.setState({ vistaembacadax: false });
     this.setState({ vomitosx: false });

                }

                else if( (dorabdominalx==true) && (urinaescurax==true)|| (dorabdominalx==true) && 
                (vomitosx==true)||(dorabdominalx==true) && (peleamareladax==true)||(dorabdominalx==true)
                 && (malestarx==true) || (vomitosx==true) && (urinaescurax==true) 
                 || (vomitosx==true) && (peleamareladax==true)|| (vomitosx==true) && (malestarx==true)
                 || (urinaescurax==true) && (peleamareladax==true)|| (urinaescurax==true) && (malestarx==true)
                 || (peleamareladax==true) && (malestarx==true)) {
                this.setState({ visible25: true });// hepatite c pequena
              this.setState({ apatiax: false });
    this.setState({ ardorlocalizadox: false });
    this.setState({ angustiax: false });
    this.setState({ ansiedadex: false });
    this.setState({ apetiteemexcessox: false });
    this.setState({ bolhaslocalizadasx: false });
    this.setState({ cansacox: false });
    this.setState({ chiadonopeitox: false });
    this.setState({coceirax: false });
    this.setState({ colicasx: false });
    this.setState({ diarreiax: false });
    this.setState({ dorabdominalx: false });
    this.setState({ dordecabecax: false });
    this.setState({ dorfacialx: false });
     this.setState({ dordeouvidox: false });
     this.setState({ doraoengolirx: false });
     this.setState({ dordegargantax: false });
     this.setState({ dormuscularx: false });
     this.setState({ dornopeitox: false });
     this.setState({ fadigax: false });
     this.setState({ faltadearfrequentex: false });
     this.setState({ faltadeapetitex: false });
     this.setState({ fraquezax: false });
     this.setState({ febrex: false });
     this.setState({ fotofobiax: false });
     this.setState({ fonofobiax: false });
     this.setState({ gasesx: false });
     this.setState({ gargantasecax: false });
     this.setState({ gargantainchadax: false });
     this.setState({ indisposicaox: false });
     this.setState({ irritabilidadex: false });
     this.setState({ insegurancax: false });
     this.setState({ malestarx: false });
     this.setState({ mauhalitox: false });
     this.setState({ manchasvermelhasx: false });
     this.setState({ nauseax: false });
     this.setState({ obstrucaonasalx: false });
     this.setState({ palidezx: false });
     this.setState({ perdadepesox: false });
     this.setState({ peleamareladax: false });
     this.setState({ sangramentonasalx: false });
     this.setState({ sonolenciax: false });
     this.setState({ sonoirregularx: false });
     this.setState({ sedeemexcessox: false });
     this.setState({ tonturax: false });
     this.setState({ tossesecax: false });
     this.setState({ ulcerasx: false });
     this.setState({ urinaescurax: false });
     this.setState({ urinaemexcesso: false });
     this.setState({ vistaembacadax: false });
     this.setState({ vomitosx: false });

              }
    }

    


    else if( (apatiax==false) && (ardorlocalizadox==false) && 
          (angustiax==false) && (ansiedadex==false) &&
          (apetiteemexcessox==false) && (bolhaslocalizadasx==false) &&
          (cansacox==false) && (chiadonopeitox==false) &&
          (coceirax==false) && (colicasx==false) &&
          (diarreiax==false) && (dorabdominalx==false) &&
          (dordecabecax==false) && (dorfacialx==false) &&
          (dordeouvidox==false) && (doraoengolirx==false) &&
          (dordegargantax==false) && (dormuscularx==false) &&
          (dornopeitox==false) && (fadigax==false) &&
          (faltadearfrequentex==false) && (faltadeapetitex==false) &&
          (fraquezax==false) && (febrex==false) &&
          (fotofobiax==false) && (fonofobiax==false) &&
          (gasesx==false) && (gargantasecax==false) &&
          (gargantainchadax==false) && (indisposicaox==false) &&
          (irritabilidadex==false) && (insegurancax==false) &&
          (malestarx==false) && (mauhalitox==false) &&
          (manchasvermelhasx==false) && (nauseax==false) &&
          (obstrucaonasalx==false) && (palidezx==false) &&
          (perdadepesox==false) && (peleamareladax==false) &&
          (sangramentonasalx==false) && (sonolenciax==false) &&
          (sonoirregularx==false) && (sedeemexcessox==false) &&
          (tonturax==false) && (tossesecax==false) &&
          (ulcerasx==false) && (urinaescurax==false) &&
          (urinaemexcessox==false) && (vistaembacadax==false) &&
          (vomitosx==false))
          {
             this.setState({ visible26: true });// nenhum sintoma
          }          

          

      
          
     
  
    }
    
     
  render() {
    const { navigate } = this.props.navigation;
   
    return (
      <View style={styles.container}>
       <ImageBackground 
    source={require('./images/back.jpg')}
    style={{flex:1,alignItems:'center',}} 
>
      
       <Modal animationType={'slide'} 
             transparent = {true} visible = {this.state.visible1}>
             <ImageBackground 
    source={require('./images/back.jpg')}
    style={{marginTop:180,flex:0.5,alignSelf:'center',
        alignItems:'center',
    justifyContent:'center'}} 
>
              <View style={{
          flex:1,
        backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3}}>
     <Text style={styles.textmodal}>Você tem grandes chances de ter gripe.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelargripegrande}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
    
  </View>
  </ImageBackground>
      </Modal>

<Modal animationType={'slide'} 
             transparent = {true} visible = {this.state.visible2}>
                          <ImageBackground 
    source={require('./images/back.jpg')}
    style={{marginTop:180,flex:0.5,alignSelf:'center',
        alignItems:'center',
    justifyContent:'center'}} 
>
              <View style={{
          flex:1,
        backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3}}>
     <Text style={styles.textmodal}>Você tem médias chances de ter gripe.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelargripemedia}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
    
  </View>
  </ImageBackground>
      </Modal>

      <Modal animationType={'slide'} 
             transparent = {true} visible = {this.state.visible3}>
             <ImageBackground 
    source={require('./images/back.jpg')}
    style={{marginTop:180,flex:0.5,alignSelf:'center',
        alignItems:'center',
    justifyContent:'center'}} 
>
              <View style={{
            flex:1,
        backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3}}>
     <Text style={styles.textmodal}>Você tem medias chances de ter sinusite.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelarsinusitemedia}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
    
  </View>
  </ImageBackground>
      </Modal>

<Modal animationType={'slide'} 
             transparent = {true} visible = {this.state.visible4}>
             <ImageBackground 
    source={require('./images/back.jpg')}
    style={{marginTop:180,flex:0.5,alignSelf:'center',
        alignItems:'center',
    justifyContent:'center'}} 
>
              <View style={{
          flex:1,
        backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3}}>
     <Text style={styles.textmodal}>Você tem pequenas chances de ter sinusite.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelarsinusitepequena}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>

  </View>
  </ImageBackground>
      </Modal>

      <Modal animationType={'slide'} 
             transparent = {true} visible = {this.state.visible5}>
             <ImageBackground 
    source={require('./images/back.jpg')}
    style={{marginTop:180,flex:0.5,alignSelf:'center',
        alignItems:'center',
    justifyContent:'center'}} 
>
              <View style={{
          flex:1,
        backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3}}>
     <Text style={styles.textmodal}>Você tem médias chances de ter anemia.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelaranemiamedia}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
    

  </View>
  </ImageBackground>
      </Modal>

      <Modal animationType={'slide'} 
             transparent = {true} visible = {this.state.visible6}>
             <ImageBackground 
    source={require('./images/back.jpg')}
    style={{marginTop:180,flex:0.5,alignSelf:'center',
        alignItems:'center',
    justifyContent:'center'}} 
>
              <View style={{
          flex:1,
        backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3}}>
     <Text style={styles.textmodal}>Você tem pequenas chances de ter anemia.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelaranemiapequena}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
              </View>
              </ImageBackground>
      </Modal>

      <Modal animationType={'slide'} 
             transparent = {true} visible = {this.state.visible7}>
             <ImageBackground 
    source={require('./images/back.jpg')}
    style={{marginTop:180,flex:0.5,alignSelf:'center',
        alignItems:'center',
    justifyContent:'center'}} 
>
              <View style={{
           flex:1,
        backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3}}>
     <Text style={styles.textmodal}>Você tem grandes chances de ter faringite.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelarfaringitegrande}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
              </View>
              </ImageBackground>
      </Modal>

      <Modal animationType={'slide'} 
             transparent = {true} visible = {this.state.visible8}>
              <ImageBackground 
    source={require('./images/back.jpg')}
    style={{marginTop:180,flex:0.5,alignSelf:'center',
        alignItems:'center',
    justifyContent:'center'}} 
>
              <View style={{
          flex:1,
        backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3}}>
     <Text style={styles.textmodal}>Você tem pequenas chances de ter faringite.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelarfaringitepequena}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
              </View>
              </ImageBackground>
      </Modal>

      <Modal animationType={'slide'} 
             transparent = {true} visible = {this.state.visible9}>
             <ImageBackground 
    source={require('./images/back.jpg')}
    style={{marginTop:180,flex:0.5,alignSelf:'center',
        alignItems:'center',
    justifyContent:'center'}} 
>
              <View style={{
           flex:1,
        backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3}}>
     <Text style={styles.textmodal}>Você tem médias chances de ter infecção intestinal.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelarinfeccaointestinalmedia}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
              </View>
              </ImageBackground>
      </Modal>

<Modal animationType={'slide'} 
             transparent = {true} visible = {this.state.visible10}>
              <ImageBackground 
    source={require('./images/back.jpg')}
    style={{marginTop:180,flex:0.5,alignSelf:'center',
        alignItems:'center',
    justifyContent:'center'}} 
>
              <View style={{
            flex:1,
        backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3}}>
     <Text style={styles.textmodal}>Você tem grandes chances de ter enxaqueca.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelarenxaquecagrande}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
              </View>
              </ImageBackground>
      </Modal>

      <Modal animationType={'slide'} 
             transparent = {true} visible = {this.state.visible11}>
               <ImageBackground 
    source={require('./images/back.jpg')}
    style={{marginTop:180,flex:0.5,alignSelf:'center',
        alignItems:'center',
    justifyContent:'center'}} 
>
              <View style={{
           flex:1,
        backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3}}>
     <Text style={styles.textmodal}>Você tem pequena chances de ter enxaqueca.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelarenxaquecapequena}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
              </View>
              </ImageBackground>
      </Modal>

      <Modal animationType={'slide'} 
             transparent = {true} visible = {this.state.visible12}>
             <ImageBackground 
    source={require('./images/back.jpg')}
    style={{marginTop:180,flex:0.5,alignSelf:'center',
        alignItems:'center',
    justifyContent:'center'}} 
>
              <View style={{
         flex:1,
        backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3}}>
     <Text style={styles.textmodal}>Você tem médias chances de ter asma.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelarasmamedia}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
              </View>
              </ImageBackground>
      </Modal>

      <Modal animationType={'slide'} 
             transparent = {true} visible = {this.state.visible13}>
              <ImageBackground 
    source={require('./images/back.jpg')}
    style={{marginTop:180,flex:0.5,alignSelf:'center',
        alignItems:'center',
    justifyContent:'center'}} 
>
              <View style={{
           flex:1,
        backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3}}>
     <Text style={styles.textmodal}>Você tem médias chances de ter depressão.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelardepressaomedia}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
              </View>
              </ImageBackground>
      </Modal>

      <Modal animationType={'slide'} 
             transparent = {true} visible = {this.state.visible14}>
             <ImageBackground 
    source={require('./images/back.jpg')}
    style={{marginTop:180,flex:0.5,alignSelf:'center',
        alignItems:'center',
    justifyContent:'center'}} 
>
              <View style={{
         flex:1,
        backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3}}>
     <Text style={styles.textmodal}>Você tem pequenas chances de ter depressão.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelardepressaopequena}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
              </View>
              </ImageBackground>

      </Modal>

      <Modal animationType={'slide'} 
             transparent = {true} visible = {this.state.visible15}>
             <ImageBackground 
    source={require('./images/back.jpg')}
    style={{marginTop:180,flex:0.5,alignSelf:'center',
        alignItems:'center',
    justifyContent:'center'}} 
>
              <View style={{
           flex:1,
        backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3}}>
     <Text style={styles.textmodal}>Você tem pequenas chances de ter diabetes.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelardiabetespequena}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
              </View>
              </ImageBackground>
      </Modal>

      <Modal animationType={'slide'} 
             transparent = {true} visible = {this.state.visible16}>
              <ImageBackground 
    source={require('./images/back.jpg')}
    style={{marginTop:180,flex:0.5,alignSelf:'center',
        alignItems:'center',
    justifyContent:'center'}} 
>
              <View style={{
          flex:1,
        backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3}}>
     <Text style={styles.textmodal}>Você tem médias chances de ter pressão alta.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelarpressaoaltamedia}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
              </View>
              </ImageBackground>
      </Modal>

      <Modal animationType={'slide'} 
             transparent = {true} visible = {this.state.visible17}>
             <ImageBackground 
    source={require('./images/back.jpg')}
    style={{marginTop:180,flex:0.5,alignSelf:'center',
        alignItems:'center',
    justifyContent:'center'}} 
>
              <View style={{
        flex:1,
        backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3}}>
     <Text style={styles.textmodal}>Você tem pequenas chances de ter pressão alta.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelarpressaoaltapequena}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
              </View>
              </ImageBackground>
      </Modal>

      <Modal animationType={'slide'} 
             transparent = {true} visible = {this.state.visible18}>
             <ImageBackground 
    source={require('./images/back.jpg')}
    style={{marginTop:180,flex:0.5,alignSelf:'center',
        alignItems:'center',
    justifyContent:'center'}} 
>
              <View style={{
         flex:1,
        backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3}}>
     <Text style={styles.textmodal}>Você tem grandes chances de ter dengue.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelardenguegrande}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
              </View>
              </ImageBackground>
      </Modal>

      <Modal animationType={'slide'} 
             transparent = {true} visible = {this.state.visible19}>
              <ImageBackground 
    source={require('./images/back.jpg')}
    style={{marginTop:180,flex:0.5,alignSelf:'center',
        alignItems:'center',
    justifyContent:'center'}} 
>
              <View style={{
         flex:1,
        backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3}}>
     <Text style={styles.textmodal}>Você tem médias chances de ter dengue.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelardenguemedia}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
              </View>
              </ImageBackground>
      </Modal>

      <Modal animationType={'slide'} 
             transparent = {true} visible = {this.state.visible20}>
             <ImageBackground 
    source={require('./images/back.jpg')}
    style={{marginTop:180,flex:0.5,alignSelf:'center',
        alignItems:'center',
    justifyContent:'center'}} 
>
              <View style={{
            flex:1,
        backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3}}>
     <Text style={styles.textmodal}>Você tem pequenas chances de ter dengue.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelardenguepequena}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
              </View>
              </ImageBackground>
      </Modal>

      <Modal animationType={'slide'} 
             transparent = {true} visible = {this.state.visible21}>
              <ImageBackground 
    source={require('./images/back.jpg')}
    style={{marginTop:180,flex:0.5,alignSelf:'center',
        alignItems:'center',
    justifyContent:'center'}} 
>
              <View style={{
         flex:1,
        backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3}}>
     <Text style={styles.textmodal}>Você tem grandes chances de ter insônia.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelarinsoniagrande}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
              </View>
              </ImageBackground>
      </Modal>

      <Modal animationType={'slide'} 
             transparent = {true} visible = {this.state.visible22}>
             <ImageBackground 
    source={require('./images/back.jpg')}
    style={{marginTop:180,flex:0.5,alignSelf:'center',
        alignItems:'center',
    justifyContent:'center'}} 
>
              <View style={{
            flex:1,
        backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3}}>
     <Text style={styles.textmodal}>Você tem grandes chances de ter herpes.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelarherpesgrande}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
              </View>
              </ImageBackground>
      </Modal>

            <Modal animationType={'slide'} 
             transparent = {true} visible = {this.state.visible23}>
               <ImageBackground 
    source={require('./images/back.jpg')}
    style={{marginTop:180,flex:0.5,alignSelf:'center',
        alignItems:'center',
    justifyContent:'center'}} 
>
              <View style={{
           flex:1,
        backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3}}>
     <Text style={styles.textmodal}>Você tem grandes chances de ter caxumba.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelarcaxumbagrande}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
              </View>
              </ImageBackground>
      </Modal>

            <Modal animationType={'slide'} 
             transparent = {true} visible = {this.state.visible24}>
               <ImageBackground 
    source={require('./images/back.jpg')}
    style={{marginTop:180,flex:0.5,alignSelf:'center',
        alignItems:'center',
    justifyContent:'center'}} 
>
              <View style={{
         flex:1,
        backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3}}>
     <Text style={styles.textmodal}>Você tem médias chances de ter hepatite C.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelarhepatitecmedia}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
              </View>
              </ImageBackground>
      </Modal>

            <Modal animationType={'slide'} 
             transparent = {true} visible = {this.state.visible25}>
             <ImageBackground 
    source={require('./images/back.jpg')}
    style={{marginTop:180,flex:0.5,alignSelf:'center',
        alignItems:'center',
    justifyContent:'center'}} 
>
              <View style={{
           flex:1,
        backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3}}>
     <Text style={styles.textmodal}>Você tem pequenas chances de ter hepatite C.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelarhepatitecpequena}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
              </View>
              </ImageBackground>
      </Modal>

            <Modal animationType={'slide'} 
             transparent = {true} visible = {this.state.visible26}>
             <ImageBackground 
    source={require('./images/back.jpg')}
    style={{marginTop:180,flex:0.5,alignSelf:'center',
        alignItems:'center',
    justifyContent:'center'}} 
>
              <View style={{
          flex:1,
        backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3}}>
     <Text style={styles.textmodal}>Nenhum sintoma marcado.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelarnenhumsintoma}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
              </View>
              </ImageBackground>
      </Modal>


     <Image style={styles.image} source={require('./images/diagnostico.png')}/>
      
      
      <View style={styles.container2}>
<ScrollView horizontal={true}>
             <TouchableOpacity style={styles.button} onPress={this.apatia} >
          <Text style={styles.textButton}>Apatia</Text>
             </TouchableOpacity>
             
             <TouchableOpacity style={styles.button} onPress={this.ardorlocalizado} >
          <Text style={styles.textButton}>Ardor Localizado</Text>
             </TouchableOpacity>
            
             <TouchableOpacity style={styles.button} onPress={this.angustia}>
          <Text style={styles.textButton}>Angústia </Text>
             </TouchableOpacity>
          
             <TouchableOpacity style={styles.button} onPress={this.ansiedade}>
          <Text style={styles.textButton}>Ansiedade</Text>
             </TouchableOpacity>
            
             <TouchableOpacity style={styles.button} onPress={this.apetiteemexcesso}>
          <Text style={styles.textButton}>Apetite em excesso</Text>
             </TouchableOpacity>

              <TouchableOpacity style={styles.button} onPress={this.bolhaslocalizadas} >
          <Text style={styles.textButton}>Bolhas Localizadas</Text>
             </TouchableOpacity>

           <TouchableOpacity style={styles.button} onPress={this.chiadonopeito} >
          <Text style={styles.textButton}>Chiado no peito</Text>
             </TouchableOpacity>
             
             <TouchableOpacity style={styles.button} onPress={this.coceira} >
          <Text style={styles.textButton}>Coceira</Text>
             </TouchableOpacity>
            
             </ScrollView>
             </View>

             

             <View style={styles.container2}>
             <ScrollView horizontal={true}>   
            
             <TouchableOpacity style={styles.button} onPress={this.colicas}>
          <Text style={styles.textButton}>Cólicas</Text>
             </TouchableOpacity>
          
             <TouchableOpacity style={styles.button} onPress={this.diarreia}>
          <Text style={styles.textButton}>Diarréia</Text>
             </TouchableOpacity>
            
             <TouchableOpacity style={styles.button} onPress={this.dorabdominal}>
          <Text style={styles.textButton}>Dor abdominal</Text>
             </TouchableOpacity>
             
    
             <TouchableOpacity style={styles.button} onPress={this.dordecabeca}>
          <Text style={styles.textButton}>Dor de cabeça</Text>
             </TouchableOpacity>

         

              <TouchableOpacity style={styles.button} onPress={this.dorfacial} >
          <Text style={styles.textButton}>Dor facial </Text>
             </TouchableOpacity>

          
              <TouchableOpacity style={styles.button} onPress={this.dordeouvido}>
          <Text style={styles.textButton}>Dor de ouvido</Text>
             </TouchableOpacity>

             <TouchableOpacity style={styles.button} onPress={this.doraoengolir} >
          <Text style={styles.textButton}>Dor ao engolir</Text>
             </TouchableOpacity>
             
             <TouchableOpacity style={styles.button} onPress={this.dordegarganta} >
          <Text style={styles.textButton}>Dor de garganta </Text>
             </TouchableOpacity>

             </ScrollView>

             </View>
             <View style={styles.container2}>
             <ScrollView horizontal={true}>

             
            
             <TouchableOpacity style={styles.button} onPress={this.dormuscular}>
          <Text style={styles.textButton}>Dor muscular</Text>
             </TouchableOpacity>
          
             <TouchableOpacity style={styles.button} onPress={this.dornopeito}>
          <Text style={styles.textButton}>Dor no peito</Text>
             </TouchableOpacity>
            
             <TouchableOpacity style={styles.button} onPress={this.fadiga}>
          <Text style={styles.textButton}>Fadiga</Text>
             </TouchableOpacity>
             
    
             <TouchableOpacity style={styles.button} onPress={this.faltadearfrequente}>
          <Text style={styles.textButton}>Falta de ar frequente</Text>
             </TouchableOpacity>

         

              <TouchableOpacity style={styles.button} onPress={this.faltadeapetite} >
          <Text style={styles.textButton}>Falta de apetite</Text>
             </TouchableOpacity>

          
              <TouchableOpacity style={styles.button} onPress={this.febre}>
          <Text style={styles.textButton}>Febre</Text>
             </TouchableOpacity>

             <TouchableOpacity style={styles.button} onPress={this.fraqueza} >
          <Text style={styles.textButton}>Fraqueza</Text>
             </TouchableOpacity>
             
             <TouchableOpacity style={styles.button} onPress={this.fotofobia} >
          <Text style={styles.textButton}>Fotofobia</Text>
             </TouchableOpacity>

             </ScrollView>
             </View>
             <View style={styles.container2}>
             <ScrollView horizontal={true}>

             
            
             <TouchableOpacity style={styles.button} onPress={this.fonofobia}>
          <Text style={styles.textButton}>Fonofobia</Text>
             </TouchableOpacity>
          
             <TouchableOpacity style={styles.button} onPress={this.gases}>
          <Text style={styles.textButton}>Gases</Text>
             </TouchableOpacity>
            
             <TouchableOpacity style={styles.button} onPress={this.gargantaseca}>
          <Text style={styles.textButton}>Garganta seca</Text>
             </TouchableOpacity>
             
    
             <TouchableOpacity style={styles.button} onPress={this.gargantainchada}>
          <Text style={styles.textButton}>Garganta inchada</Text>
             </TouchableOpacity>

         

              <TouchableOpacity style={styles.button} onPress={this.indisposicao} >
          <Text style={styles.textButton}>Indisposição</Text>
             </TouchableOpacity>

          
              <TouchableOpacity style={styles.button} onPress={this.irritabilidade}>
          <Text style={styles.textButton}>Irritabilidade</Text>
             </TouchableOpacity>

               <TouchableOpacity style={styles.button} onPress={this.inseguranca} >
          <Text style={styles.textButton}>Insegurança</Text>
             </TouchableOpacity>
             
             <TouchableOpacity style={styles.button} onPress={this.malestar} >
          <Text style={styles.textButton}>Mal-estar</Text>
             </TouchableOpacity>
             </ScrollView>
             </View>
             <View style={styles.container2}>
             <ScrollView horizontal={true}>

           
            
             <TouchableOpacity style={styles.button} onPress={this.mauhalito}>
          <Text style={styles.textButton}>Mau hálito</Text>
             </TouchableOpacity>
          
             <TouchableOpacity style={styles.button} onPress={this.manchasvermelhas}>
          <Text style={styles.textButton}>Manchas vermelhas</Text>
             </TouchableOpacity>
            
             <TouchableOpacity style={styles.button} onPress={this.nausea}>
          <Text style={styles.textButton}>Náuseas</Text>
             </TouchableOpacity>
             
    
             <TouchableOpacity style={styles.button} onPress={this.obstrucaonasal}>
          <Text style={styles.textButton}>Obstrução nasal </Text>
             </TouchableOpacity>

         

              <TouchableOpacity style={styles.button} onPress={this.palidez} >
          <Text style={styles.textButton}>Palidez</Text>
             </TouchableOpacity>

          
              <TouchableOpacity style={styles.button} onPress={this.perdadepeso}>
          <Text style={styles.textButton}>Perda de peso</Text>
             </TouchableOpacity>
                   <TouchableOpacity style={styles.button} onPress={this.peleamarelada} >
          <Text style={styles.textButton}>Pele amarelada </Text>
             </TouchableOpacity>
             
             <TouchableOpacity style={styles.button} onPress={this.sangramentonasal} >
          <Text style={styles.textButton}>Sangramento nasal</Text>
             </TouchableOpacity>

             </ScrollView>
             </View>
<View style={styles.container2}>
             <ScrollView horizontal={true}>

       
            
             <TouchableOpacity style={styles.button} onPress={this.sonolencia}>
          <Text style={styles.textButton}>Sonolência</Text>
             </TouchableOpacity>
          
             <TouchableOpacity style={styles.button} onPress={this.sonoirregular}>
          <Text style={styles.textButton}>Sono irregular</Text>
             </TouchableOpacity>
            
             <TouchableOpacity style={styles.button} onPress={this.sedeemexcesso}>
          <Text style={styles.textButton}>Sede em excesso</Text>
             </TouchableOpacity>
             
    
             <TouchableOpacity style={styles.button} onPress={this.tontura}>
          <Text style={styles.textButton}>Tontura</Text>
             </TouchableOpacity>

         

              <TouchableOpacity style={styles.button} onPress={this.tosseseca} >
          <Text style={styles.textButton}>Tosse seca</Text>
             </TouchableOpacity>

          
              <TouchableOpacity style={styles.button} onPress={this.ulceras}>
          <Text style={styles.textButton}>Úlceras</Text>
             </TouchableOpacity>

                 <TouchableOpacity style={styles.button} onPress={this.urinaescura} >
          <Text style={styles.textButton}>Urina escura</Text>
             </TouchableOpacity>
             
             <TouchableOpacity style={styles.button} onPress={this.urinaemexcesso} >
          <Text style={styles.textButton}>Urina em excesso</Text>
             </TouchableOpacity>

                   <TouchableOpacity style={styles.button} onPress={this.vomitos}>
          <Text style={styles.textButton}>Vômitos </Text>
             </TouchableOpacity>
             </ScrollView>
             </View>
             
             <View style={styles.container3}>

             <TouchableOpacity style={styles.button2} onPress={this.desmarcartudo} >
          <Text style={styles.textButton2}> Desmarcar tudo </Text>
             </TouchableOpacity>

              <TouchableOpacity style={styles.button5} onPress={() => this.props.navigation.navigate('Menu')}>
          <Text style={styles.textButton5}> Voltar </Text>
             </TouchableOpacity>

             <TouchableOpacity style={styles.button6} onPress={this.diagnostico}>
          <Text style={styles.textButton6}> Fazer diagnóstico  </Text>
             </TouchableOpacity>
           </View>
          
             </ImageBackground>

             
        </View>




    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ACCEE8',
    flex: 1,

    },
    container2: {
    flexDirection:'row',
    marginTop:8,
    top:-85,
    },

  inner: {
  justifyContent: 'center', 
  alignItems:'center'
  
  },

  image: { 
    width:  250, 
    height: 250,
    marginTop:-80
      },
         buttonmodal:{
     height: 37,
    width: 140,
    padding: 15,   
    backgroundColor: '#FF7A7D',
    borderColor: '#ff6505', 
    borderRadius:25,
    borderColor:'#fff',
    borderWidth:2,
    top:100,
    left:170
    

  },
       buttonmodal2:{
     height: 30,
    width: 100,
    padding: 15,
    backgroundColor: '#FF7A7D',
    margin:15,
    borderColor: '#FF7A7D',
    alignSelf:'flex-end',
    marginTop:70
    

  },
    buttonmodal4:{
     height: 30,
    width: 100,
    padding: 15,
    backgroundColor: '#FF7A7D',
    margin:15,
    borderColor: '#FF7A7D',
    
    marginTop:70
    

  },
   buttonmodal3:{
     height: 30,
    width: 100,
    padding: 15,
    backgroundColor: '#FF7A7D',
    
    borderColor: '#FF7A7D',
    top:-100
    
    

  },
   textmodal: {
    color: '#000',
    fontWeight: '700',
    fontSize: 25
    

  },
    textbuttonmodal: {
       color: '#fff',
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 18,
    marginTop:-12
    

    

  },

      image2: { marginTop:-205,
    left:5,
    width:  200, 
    height: 210,
      },

      container3: {
   
    flex:0.6,
     },

  container4: {
    
    flex:0.7,
    
  },

  container5: {
    
    flex:1,
    
  },


button: {
    height: 33,
    width: 165,
    top:10,
    margin:10,
    backgroundColor: '#FF7A7D',
    flexDirection:'column',
    borderColor: '#ff6505', 
    borderRadius:25,
     borderColor:'#000',
    borderWidth:1
    


  },

  button2: {
    height: 50,
    margin:10,
    width: 150,
    padding: 15,
    backgroundColor: '#FF7A7D',
    borderRadius:100,
    left:120,
    marginTop:-50,
    borderColor:'#fff',
    borderWidth:2
    
    
    

  },
  button3: {
    height: 25,
    marginTop:10,
    top:34,
    left:80,
    width: 150, 
    backgroundColor: '#FF7A7D', 
    borderColor: '#ff6505', 
    borderRadius:25,
    


  },
  button4: {
    height: 25,
    top:34,   
    left:80,
    width: 150,  
    backgroundColor: '#FF7A7D', 
    borderColor: '#ff6505', 
    borderRadius:25,
    


  },
    button5: {
    height: 50,
    marginTop:10,
    top:-69,
    right:35,
    width: 150, 
    backgroundColor: '#FF7A7D',
    borderColor: '#ff6505', 
    borderRadius:25,
    borderColor:'#fff',
    borderWidth:2
  },
      button6: {
    height: 50,
    marginTop:10,
    top:-67,
    width: 260, 
    backgroundColor: '#FF7A7D',
    borderColor: '#ff6505', 
    borderRadius:25,
    borderColor:'#fff',
    borderWidth:2
  },
  textButton: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: '800',
    fontSize: 15,
    marginTop:5
    
  },
   text: {
    color: '#000',
    textAlign: 'center',
    fontWeight: '900',
    fontSize: 20,
    marginTop:-120
  },
    textButton2: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 20,
    marginTop:-18

  },
    textButton5: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 20,
    marginTop:10

  },
      textButton6: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 23,
    marginTop:5

  },
});

 export default Page5;
