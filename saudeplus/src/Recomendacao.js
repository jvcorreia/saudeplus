import React, { Component } from 'react';
import { Text, View, Image, TextInput, TouchableOpacity,
 StyleSheet ,Alert, Modal, ScrollView,ImageBackground} from 'react-native';
import firebase from 'react-native-firebase';


 class Page6 extends Component {
   static navigationOptions = {
    header: null,
  };

state = { visible1:false,visible2:false,visible3:false,visible4:false,visible5:false,
 visible6:false,visible7:false, visible8:false, visible9:false, visible10:false, visible11:false, 
 visible12:false, visible13:false, visible14:false,visible15:false}

anemia = () => {
    this.setState({ visible1: true });
  }

asma = () => {
    this.setState({ visible2: true });
  }

caxumba = () => {
    this.setState({ visible3: true });
  }

dengue = () => {
    this.setState({ visible4: true });
  }

depressao = () => {
    this.setState({ visible5: true });
  }

diabetes = () => {
    this.setState({ visible6: true });
  }

enxaqueca = () => {
    this.setState({ visible7: true });
  }

faringite = () => {
    this.setState({ visible8: true });
  }

gripe = () => {
    this.setState({ visible9: true});
  }

herpes = () => {
    this.setState({ visible10: true});
  }

hepatitec= () => {
    this.setState({ visible11: true});
  }

pressaoalta = () => {
    this.setState({ visible12: true});
  }

infeccaointestinal = () => {
    this.setState({ visible13: true});
  }

insonia = () => {
    this.setState({ visible14: true});
  }

sinusite = () => {
    this.setState({ visible15: true});
  }


cancelaranemia = () => {
    this.setState({ visible1: false });
  }

cancelarasma = () => {
    this.setState({ visible2: false });
  }

cancelarcaxumba = () => {
    this.setState({ visible3: false });
  }

cancelardengue = () => {
    this.setState({ visible4: false });
  }

cancelardepressao = () => {
    this.setState({ visible5: false });
  }

clinicasproximasdepressao = () => {
    this.setState({ visible5: false });
    this.props.navigation.navigate('ClinicaPsicologiaMaps');
  }

chatsonho = () => {
  this.setState({ visible5: false });
  this.props.navigation.navigate('ChatSonho');
}

hospitaisproximosfaringite = () => {
    this.setState({ visible8: false });
    this.props.navigation.navigate('HospitalMaps');
  }

  hospitaisproximoshepaticec = () => {
    this.setState({ visible11: false });
    this.props.navigation.navigate('HospitalMaps');
  }

hospitaisproximosinfeccaointestinal = () => {
    this.setState({ visible13: false });
    this.props.navigation.navigate('HospitalMaps');
  }


cancelardiabetes = () => {
    this.setState({ visible6: false });
  }

cancelarenxaqueca = () => {
    this.setState({ visible7: false });
  }

cancelarfaringite = () => {
    this.setState({ visible8: false });
  }

cancelargripe = () => {
    this.setState({ visible9: false });
  }

cancelarherpes = () => {
    this.setState({ visible10: false });
  }

cancelarhepatitec = () => {
    this.setState({ visible11: false });
  }

cancelarpressaoalta = () => {
    this.setState({ visible12: false });
  }

cancelarinfeccaointestinal = () => {
    this.setState({ visible13: false });
  }

cancelarinsonia = () => {
    this.setState({ visible14: false });
  }

cancelarsinusite = () => {
    this.setState({ visible15: false });
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>

         <ImageBackground 
    source={require('./images/back.jpg')}
    style={{flex:1,alignItems:'center',}} 
          >


      <Modal animationType={'slide'} transparent = {true} visible = {this.state.visible1}>
               <ImageBackground 
    source={require('./images/back.jpg')}
    style={{flex:1,alignSelf:'center',
            alignItems:'center',
            justifyContent:'center'}} 
               >
              <View style={{
                flex:1,
            backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3,
          alignSelf:'center',
          }}>
     <Text style={styles.textmodal}>Para tratar a anemia é necessário aumentar a quantidade de
      hemoglobina na corrente sanguínea, que é um componente do sangue que carrega oxigênio.
      Para isso, é importante aumentar o consumo de alimentos ricos em ferro e ácido fólico,
      pois eles ajudam o corpo a produzir mais hemoglobina e glóbulos vermelhos.</Text>
      <Text style={styles.textmodal2}>Alguns alimentos ricos em ferro: {"\n"} -Carne, 
      com ênfase para o fígado; {"\n"} -Açaí; {"\n"} -Pão de cevada; {"\n"} -Castanhas-do-pará.</Text>
      <Text style={styles.textmodal2}>Remédios mais usados: {"\n"} -Combiron, preço médio: 30 reais; {"\n"} -Noripurum, preço médio: 40 reais; {"\n"}
       -Redoxon, preço médio: 15 reais; {"\n"} -Ferronil, preço médio: 13 reais.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelaranemia}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
    
          </View>
        </ImageBackground>
      </Modal>

           <Modal animationType={'slide'} transparent = {true} visible = {this.state.visible2}>
               <ImageBackground 
    source={require('./images/back.jpg')}
    style={{flex:1,alignSelf:'center',
            alignItems:'center',
            justifyContent:'center'}} 
               >
              <View style={{
                flex:1,
            backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3,
          alignSelf:'center',
          }}>
     <Text style={styles.textmodal}>A asma não tem cura, mas com o tratamento adequado os sintomas
      podem melhorar e até mesmo desaparecer ao longo do tempo. Por isso é fundamental fazer
       acompanhamento médico correto e constante, a maioria das pessoas com asma pode levar uma vida
        absolutamente normal.</Text>
      <Text style={styles.textmodal2}>Alguns fatores para evitar asma: {"\n"} -Pratique atividades físicas regularmente;
       {"\n"} -Evite acúmulo de sujeira ou poeira; {"\n"} -Não fume; {"\n"} -Tenha alimentação saudável.</Text>
      <Text style={styles.textmodal2}>Remédios mais usados: {"\n"} -Salbutamol, preço médio: 30 reais; {"\n"} -Formoterol, preço médio: 45 reais; {"\n"}
       -Prednisona, preço médio: 15 reais; {"\n"} -Budesonida, preço médio: 15 reais.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelarasma}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
    
          </View>
        </ImageBackground>
      </Modal>
       
      <Modal animationType={'slide'} transparent = {true} visible = {this.state.visible3}>
               <ImageBackground 
    source={require('./images/back.jpg')}
    style={{flex:1,alignSelf:'center',
            alignItems:'center',
            justifyContent:'center'}} 
               >
              <View style={{
                flex:1,
            backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3,
          alignSelf:'center',
          }}>
     <Text style={styles.textmodal}>Por ser uma infecção viral, a caxumba é tratada naturalmente
      pelo organismo. A indicação é apenas de repouso, medicamentos para dor e temperatura e
       observação cuidadosa para a possibilidade de aparecimento de complicações.</Text>
      <Text style={styles.textmodal2}>Alguns fatores para evitar caxumba: {"\n"} -Repouso e hidratação;
       {"\n"} -Alimentação mole e pastosa; {"\n"} -Cuidar da higiene bucal; {"\n"} -Aplicar compressas mornas sobre o inchaço.</Text>
      <Text style={styles.textmodal2}>Remédios caseiros mais usados: {"\n"} -Figueira sagrada; {"\n"} -Gengibre; {"\n"}
       -Pimenta preta; {"\n"} -Bicarbonato de sódio.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelarcaxumba}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
    
          </View>
        </ImageBackground>
      </Modal>

       <Modal animationType={'slide'} transparent = {true} visible = {this.state.visible4}>
               <ImageBackground 
    source={require('./images/back.jpg')}
    style={{flex:1,alignSelf:'center',
            alignItems:'center',
            justifyContent:'center'}} 
               >
              <View style={{
                flex:1,
            backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3,
          alignSelf:'center',
          }}>
     <Text style={styles.textmodal}>Não existe tratamento específico para a dengue
     . Em caso de suspeita é fundamental procurar um profissional de saúde para o correto
      diagnóstico.</Text>
      <Text style={styles.textmodal2}>Alguns fatores para evitar caxumba: {"\n"} -Manter higiene geral da casa;
       {"\n"} -Evitar a água parada; {"\n"} -Manter fechado barris e tonéis de água; {"\n"} -Manter as calhas limpas.</Text>
      <Text style={styles.textmodal2}>Algumas formas de tratamento: {"\n"} -Ingerir bastante líquido; {"\n"} -Não tomar medicamentos por conta própria; {"\n"}
         -Fazer repouso.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelardengue}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
    
          </View>
        </ImageBackground>
      </Modal>

       <Modal animationType={'slide'} transparent = {true} visible = {this.state.visible5}>
               <ImageBackground 
    source={require('./images/back.jpg')}
    style={{flex:1,alignSelf:'center',
            alignItems:'center',
            justifyContent:'center'}} 
               >
              <View style={{
                flex:1,
            backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3,
          alignSelf:'center',
          }}>
     <Text style={styles.textmodal}>O tratamento da depressão é essencialmente medicamentoso.
      Existem mais de 30 antidepressivos disponíveis. Ao contrário do que alguns temem, essas
       medicações não são como drogas, que deixam a pessoa eufórica e provocam vício.</Text>
       <Text style={styles.textmodal}>De nenhuma forma, a pessoa que se sentir algum desses
       sintomas deve estar sozinha, todo apoio possível é necessário pois depressão é real.</Text>
       <TouchableOpacity style={styles.buttonmodal} onPress={this.clinicasproximasdepressao}>
              <Text style={styles.textbuttonmodal}> Clínicas próximas </Text>
              </TouchableOpacity>
               <TouchableOpacity style={styles.buttonmodal} onPress={this.chatsonho}>
              <Text style={styles.textbuttonmodal}> Quero conversar com alguém </Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelardepressao}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
    
          </View>
        </ImageBackground>
      </Modal>

      <Modal animationType={'slide'} transparent = {true} visible = {this.state.visible6}>
               <ImageBackground 
    source={require('./images/back.jpg')}
    style={{flex:1,alignSelf:'center',
            alignItems:'center',
            justifyContent:'center'}} 
               >
              <View style={{
                flex:1,
            backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3,
          alignSelf:'center',
          }}>
     <Text style={styles.textmodal}>O tratamento de diabetes tem como objetivo controlar a
      glicose presente no sangue do paciente evitando que apresenta picos ou quedas ao longo
       do dia. Para o tratamento do diabetes, a SES disponibiliza
        medicamentos e insumos para o tratamento e monitoramento da doença.</Text>
     
             <Text style={styles.textmodal2}>Para evitar a diabetes: {"\n"} -Tenha uma alimentação equilibrada;
       {"\n"} -Afaste o sedentarismo; {"\n"} -Controle o peso; {"\n"} -Não esqueça dos check-ups.</Text>
      <Text style={styles.textmodal2}>Remédios mais indicados: {"\n"} -Azukon MR, preço médio: 15 reais.; {"\n"}
       -Galvus, preço médio: 178 reais; {"\n"}
         -Januvia, preço médio: 122 reais.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelardiabetes}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
    
          </View>
        </ImageBackground>
      </Modal>

      <Modal animationType={'slide'} transparent = {true} visible = {this.state.visible7}>
               <ImageBackground 
    source={require('./images/back.jpg')}
    style={{flex:1,alignSelf:'center',
            alignItems:'center',
            justifyContent:'center'}} 
               >
              <View style={{
                flex:1,
            backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3,
          alignSelf:'center',
          }}>
     <Text style={styles.textmodal}>Antes de iniciar o tratamento para enxaqueca, é necessário
      saber se o diagnóstico está correto e qual o fator desencadeante dela. No geral, o melhor
      é evitar esses desencadeantes e tomar o medicamento indicado pelo médico quando uma crise
       aparecer.</Text>
     
             <Text style={styles.textmodal2}>Para evitar a enxaqueca: {"\n"} -Reduza seu estresse;
       {"\n"} -Estabeleça horários regulares de sono; {"\n"} -Evite ruídos altos e luzes brilhantes; {"\n"} -Coma regularmente.</Text>
      <Text style={styles.textmodal2}>Remédios mais indicados: {"\n"} -Amato, preço médio: 45 reais.; {"\n"}
       -Buscofem, preço médio: 17 reais; {"\n"}
         -Dorflex, preço médio: 4 reais.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelarenxaqueca}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
    
          </View>
        </ImageBackground>
      </Modal>

      <Modal animationType={'slide'} transparent = {true} visible = {this.state.visible8}>
               <ImageBackground 
    source={require('./images/back.jpg')}
    style={{flex:1,alignSelf:'center',
            alignItems:'center',
            justifyContent:'center'}} 
               >
              <View style={{
                flex:1,
            backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3,
          alignSelf:'center',
          }}>
     <Text style={styles.textmodal}>Para a faringite bacteriana, é necessário consultar um
      médico para saber também qual o melhor tratamento disponível para cada caso. Geralmente,
       ele é feito à base de antibióticos e analgésicos. Já para a Faringite causada por vírus
        geralmente não demanda muitos cuidados nem um tratamento específico. Em caso de qualquer suspeita,
         é aconselhado procurar um médico o mais rápido possível.</Text>
     
             <TouchableOpacity style={styles.buttonmodal} onPress={this.hospitaisproximosfaringite}>
              <Text style={styles.textbuttonmodal}> Hospitais próximos </Text>
              </TouchableOpacity>
      <Text style={styles.textmodal2}>Remédios mais indicados (para alguns, é necessária a receita médica): {"\n"}
       -Astro, preço médio: 20 reais.; {"\n"}
       -Nimesulida, preço médio: 6 reais; {"\n"}
         -Amoxicilina, preço médio: 40 reais.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelarfaringite}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
    
          </View>
        </ImageBackground>
      </Modal>

      <Modal animationType={'slide'} transparent = {true} visible = {this.state.visible9}>
               <ImageBackground 
    source={require('./images/back.jpg')}
    style={{flex:1,alignSelf:'center',
            alignItems:'center',
            justifyContent:'center'}} 
               >
              <View style={{
                flex:1,
            backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3,
          alignSelf:'center',
          }}>
     <Text style={styles.textmodal}>Ainda não existem medicamentos que tenham demonstrado bons
      resultados no combate aos vírus da gripe e do resfriado. Por isso, o tratamento é
       direcionado ao alívio dos sintomas da gripe. Os principais medicamentos sintomáticos
        utilizados são os analgésicos e antitérmicos, que aliviam a dor e a febre.</Text>
     
             <Text style={styles.textmodal2}>Remédios caseiros: {"\n"} -Xarope de alho;
       {"\n"} -Suco de maçã com mel; {"\n"} -Xarope de alho; {"\n"} -Suco de caju.</Text>
      <Text style={styles.textmodal2}>Remédios mais indicados: {"\n"} -Aspirina, preço médio: 15 reais.; {"\n"}
       -Multigrip, preço médio: 8 reais; {"\n"}
         -Paracetamol, preço médio: 4 reais.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelargripe}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
    
          </View>
        </ImageBackground>
      </Modal>

<Modal animationType={'slide'} transparent = {true} visible = {this.state.visible10}>
               <ImageBackground 
    source={require('./images/back.jpg')}
    style={{flex:1,alignSelf:'center',
            alignItems:'center',
            justifyContent:'center'}} 
               >
              <View style={{
                flex:1,
            backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3,
          alignSelf:'center',
          }}>
     <Text style={styles.textmodal}>Alguns casos de herpes são leves e não precisam de tratamento
      a não ser tratamentos tópicos. Mas pessoas que têm surtos graves ou prolongados
       (principalmente se for o primeiro episódio de infecção), que têm problemas no sistema
        imunológico ou aquelas que têm recorrência frequente talvez precisem fazer uso de
         medicamentos antivirais. Pacientes com recorrências graves ou frequentes de herpes
          oral ou genital podem optar por continuar com os medicamentos antivirais para reduzir
           a frequência e a gravidade dessas recorrências.</Text>
     
      <Text style={styles.textmodal2}>Atenção: {"\n"} Somente um médico pode dizer
       qual o medicamento mais indicado para o seu caso.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelarherpes}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
    
          </View>
        </ImageBackground>
      </Modal>

            <Modal animationType={'slide'} transparent = {true} visible = {this.state.visible11}>
               <ImageBackground 
    source={require('./images/back.jpg')}
    style={{flex:1,alignSelf:'center',
            alignItems:'center',
            justifyContent:'center'}} 
               >
              <View style={{
                flex:1,
            backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3,
          alignSelf:'center',
          }}>
     <Text style={styles.textmodal}>Nem sempre há necessidade de tratamento. O médico saberá
      dizer se o seu caso exigirá terapia ou não. Geralmente, mesmo para pessoas que dispensam
       o tratamento, exames de sangue de acompanhamento são solicitados.</Text>
     
          <TouchableOpacity style={styles.buttonmodal} onPress={this.hospitaisproximoshepaticec}>
              <Text style={styles.textbuttonmodal}> Hospitais próximos </Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelarhepatitec}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
              </View>
              </ImageBackground>
              </Modal>

<Modal animationType={'slide'} transparent = {true} visible = {this.state.visible12}>
               <ImageBackground 
    source={require('./images/back.jpg')}
    style={{flex:1,alignSelf:'center',
            alignItems:'center',
            justifyContent:'center'}} 
               >
              <View style={{
                flex:1,
            backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3,
          alignSelf:'center',
          }}>
     <Text style={styles.textmodal}>Uma pequena parcela de hipertensos
      consegue dominar a doença apenas com ajustes no cardápio, exercícios físicos e controle do
       estresse. Para tomar a decisão de não entrar imediatamente com medicamentos, o médico se
        baseia em bons resultados gerais de exames como glicemia e colesterol e se os rins estão
         funcionando direito.</Text>
     
             <Text style={styles.textmodal2}>Para evitar a pressão alta: {"\n"} -Pratique exercícios físicos;
       {"\n"} -Evite alimentos frituras e doces; {"\n"} -Evite o estresse.</Text>
      <Text style={styles.textmodal2}>Remédios mais indicados: {"\n"} -Benazepril, preço médio: 55 reais.; {"\n"}
       -Valsartana, preço médio: 40 reais; {"\n"}
         -Furosemida, preço médio: 12 reais.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelarpressaoalta}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
    
          </View>
        </ImageBackground>
      </Modal>

<Modal animationType={'slide'} transparent = {true} visible = {this.state.visible13}>
               <ImageBackground 
    source={require('./images/back.jpg')}
    style={{flex:1,alignSelf:'center',
            alignItems:'center',
            justifyContent:'center'}} 
               >
              <View style={{
                flex:1,
            backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3,
          alignSelf:'center',
          }}>
     <Text style={styles.textmodal}>O tratamento para uma infecção intestinal deve ser
      sempre orientado por um clínico geral ou um gastroenterologista, pois é preciso
       identificar o tipo de microorganismo que está causando a infecção e, só depois,
        iniciar o tratamento mais adequado.</Text>
        <TouchableOpacity style={styles.buttonmodal} onPress={this.hospitaisproximosinfeccaointestinal} >
              <Text style={styles.textbuttonmodal}> Hospitais próximos </Text>
              </TouchableOpacity>
     
             <Text style={styles.textmodal2}>Cuidados: {"\n"} -Ficar em repouso ;
       {"\n"} -Evitar alimentos de difícil digestão; {"\n"} -Beber muitos líquidos;
       {"\n"}- Lavar e cozinhar bem os alimentos.</Text>

              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelarinfeccaointestinal}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
    
          </View>
        </ImageBackground>
      </Modal>

      <Modal animationType={'slide'} transparent = {true} visible = {this.state.visible14}>
               <ImageBackground 
    source={require('./images/back.jpg')}
    style={{flex:1,alignSelf:'center',
            alignItems:'center',
            justifyContent:'center'}} 
               >
              <View style={{
                flex:1,
            backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3,
          alignSelf:'center',
          }}>
     <Text style={styles.textmodal}>Uma mudança nos hábitos de sono e tratar as causas
      subjacentes da insônia, como condições médicas ou medicamentos, pode restaurar um padrão
       de sono saudável em muitos pacientes. Se essas medidas não funcionarem, o médico pode
        recomendar medicamentos para ajudar com o relaxamento e na readequação do sono.</Text>
     
             <Text style={styles.textmodal2}>Para evitar a insônia: {"\n"} - Evite luz à noite;
       {"\n"} -Sono regular; {"\n"} -Evite a cafeína a noite.</Text>
      <Text style={styles.textmodal2}>Remédios mais indicados: {"\n"} -Amitriptilina, preço médio: 13 reais.; {"\n"}
       -Nortriptilina, preço médio: 19 reais; {"\n"}
         -Clonazepam, preço médio: 12 reais.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelarinsonia}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
    
          </View>
        </ImageBackground>
      </Modal>

      <Modal animationType={'slide'} transparent = {true} visible = {this.state.visible15}>
               <ImageBackground 
    source={require('./images/back.jpg')}
    style={{flex:1,alignSelf:'center',
            alignItems:'center',
            justifyContent:'center'}} 
               >
              <View style={{
                flex:1,
            backgroundColor:'#transparent',
          borderColor:'#000',
          borderWidth:3,
          alignSelf:'center',
          }}>
     <Text style={styles.textmodal}>
A sinusite aguda geralmente não requer nenhum tratamento além do alívio sintomático
 com medicamentos para a dor, descongestionantes nasais e soro para lavagem nasal. 
 Já a sinusite crônica pode exigir antibióticos.</Text>
     
             <Text style={styles.textmodal2}>Para evitar a sinusite: {"\n"} -Inale vapor;
       {"\n"} -Faça lavagem nasal; {"\n"}-Elimine alérgenos do ambiente-Chá de camomila; {"\n"} -Proteja-se do frio.</Text>
      <Text style={styles.textmodal2}>Remédios caseiros: {"\n"}-Suco de espinafre;{"\n"}
       -Nebulização com eucalipto, sal e água; {"\n"}
         -Inalação com cebola fervida.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.cancelarsinusite}>
              <Text style={styles.textbuttonmodal}> Obrigado </Text>
              </TouchableOpacity>
    
          </View>
        </ImageBackground>
      </Modal>

       <View style={styles.image}> 

      <Image style={styles.image} source={require('./images/recomendacoes.png')}/> 
      </View>     
        <Text style={styles.text}> Para qual mal vou querer recomendações? Role para baixo e procure o problema
        cujo você quer ser auxiliado! </Text>
        
      <ScrollView>
         
                 <TouchableOpacity style={styles.button} onPress={this.anemia} >
          <Text style={styles.textButton} 
          > Suspeita de Anemia  </Text>
        </TouchableOpacity>

                 <TouchableOpacity style={styles.button} onPress={this.asma} >
          <Text style={styles.textButton} 
          >Suspeita de Asma </Text>
        </TouchableOpacity>

                 <TouchableOpacity style={styles.button} onPress={this.caxumba} >
          <Text style={styles.textButton} 
          >Suspeita de Caxumba </Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.button} onPress={this.dengue} >
          <Text style={styles.textButton} 
          >Suspeita de Dengue </Text>
        </TouchableOpacity>

         <TouchableOpacity style={styles.button} onPress={this.depressao} >
          <Text style={styles.textButton} 
          >Suspeita de Depressão </Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.button} onPress={this.diabetes} >
          <Text style={styles.textButton} 
          >Suspeita de Diabetes</Text>
        </TouchableOpacity>

<TouchableOpacity style={styles.button} onPress={this.enxaqueca} >
          <Text style={styles.textButton} 
          >Suspeita de Enxaqueca </Text>
        </TouchableOpacity>

<TouchableOpacity style={styles.button} onPress={this.faringite} >
          <Text style={styles.textButton} 
          >Suspeita de Faringite </Text>
        </TouchableOpacity>

<TouchableOpacity style={styles.button} onPress={this.gripe} >
          <Text style={styles.textButton} 
          >Suspeita de Gripe </Text>
        </TouchableOpacity>

<TouchableOpacity style={styles.button} onPress={this.herpes} >
          <Text style={styles.textButton} 
          >Suspeita de Herpes </Text>
        </TouchableOpacity>

<TouchableOpacity style={styles.button} onPress={this.hepatitec} >
          <Text style={styles.textButton} 
          >Suspeita de Hepatite C </Text>
        </TouchableOpacity>

<TouchableOpacity style={styles.button} onPress={this.pressaoalta} >
          <Text style={styles.textButton} 
          >Suspeita de Pressão alta </Text>
        </TouchableOpacity>

<TouchableOpacity style={styles.button} onPress={this.infeccaointestinal} >
          <Text style={styles.textButton} 
          >Suspeita de Infecção Intestinal </Text>
        </TouchableOpacity>

<TouchableOpacity style={styles.button} onPress={this.insonia} >
          <Text style={styles.textButton} 
          >Suspeita de Insônia </Text>
        </TouchableOpacity>

<TouchableOpacity style={styles.button} onPress={this.sinusite} >
          <Text style={styles.textButton} 
          >Suspeita de Sinusite </Text>
        </TouchableOpacity>

        </ScrollView>

                 <TouchableOpacity style={styles.button2} onPress={() => this.props.navigation.navigate('Menu')} >
          <Text style={styles.textButton2} 
          > Voltar ao menu </Text>
        </TouchableOpacity>
             
              </ImageBackground> 
        </View>



    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ACCEE8',
    flex: 1,
    
    
    
  },
 image2: { right:250,
    width:  250, 
    height: 200,
    alignItems:'center',
    alignSelf:'center'
      },
       
 

  image: {  
    width:  305, 
    height: 305,
    marginTop:-20
      },

      text: {  
    fontSize:16,
    color:'#000',
    fontWeight:'900',
    marginTop:-120,
    textAlign: 'center',  
    margin:10    
  },

      image2: {
    left:210,
    top:45,
    width:  150, 
    height: 150,
    alignItems:'flex-end',
    justifyContent:'flex-end'
      },
      button: {
    height: 30,
    width: 200,
    padding: 15,
    backgroundColor: '#FF7A7D',
    margin:10,
    justifyContent:'center',
    alignItems:'center',
    borderColor: '#ff6505', 
    borderRadius:25,
    borderColor:'#fff',
    borderWidth:2
    


  },
   button2: {
    height: 37,
    width: 350,
    padding: 15,
    backgroundColor: '#FF7A7D',  
    borderColor: '#ff6505', 
    borderRadius:25,
    margin:15,
    borderColor:'#fff',
    borderWidth:2
  },
      textButton: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 15, 
  },
    textButton2: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 18,
    marginTop:-10
   
  },
   buttonmodal:{
     height: 36,
    width: 280,
    padding: 15,
    backgroundColor: '#FF7A7D',
    margin:15,
    borderColor:'#fff',
    borderWidth:2,
    left:26
  },
       buttonmodal2:{
     height: 30,
    width: 100,
    padding: 15,
    backgroundColor: '#FF7A7D',
    margin:15,
    borderColor: '#FF7A7D',
    alignSelf:'flex-end',
    marginTop:70
    

  },
   buttonmodal3:{
     height: 30,
    width: 100,
    padding: 15,
    backgroundColor: '#FF7A7D',
    
    borderColor: '#FF7A7D',
    alignSelf:'flex-end',
    
    

  },
   textmodal: {
    color: '#000',
    fontWeight: '700',
    fontSize: 20,
    borderColor:'#fff',
    borderWidth:3,
    margin:10,
    paddingLeft: 4

  },
   textmodal2: {
    color: '#000',
    fontWeight: '700',
    fontSize: 20,
    margin:7,
    borderColor:'#fff',
    borderWidth:3,
    paddingLeft: 4,
    paddingRight: 4
  },
    textbuttonmodal: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 18,
    marginTop:-10,
    

    

  },



});

 export default Page6;