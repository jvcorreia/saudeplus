import React, { Component } from 'react';
import {StyleSheet, View, Image,Text,TextInput, TouchableOpacity,Alert,CheckBox,Modal,
 ScrollView, ImageBackground} from 'react-native';
import firebase from 'react-native-firebase';


 class Page2 extends Component {
     static navigationOptions = {
    header: null,
  };

state = {email: '', password: "",password2: "",check:false, visible1:false, visible2:false,
 visible3:false, visible4:false,visible5:false,visible6:false}

showModal1 = () => {
    this.setState({ visible1: true });
  }
handleCancel1 = () => {
    this.setState({ visible1: false });
  }
handleCancel2 = () => {
    this.setState({ visible2: false });
  }
handleCancel3 = () => {
    this.setState({ visible3: false });
  }
handleCancel4 = () => {
    this.setState({ visible4: false });
  }
handleCancel5 = () => {
    this.setState({ visible5: false });
    this.props.navigation.navigate('Login');
  }
handleCancel6 = () => {
    this.setState({ visible6: false });
  }
handleSignUp = () => { const {email, password, password2, check, visible1,visible2,
  visible3,visible4,visible5, visible6} = this.state;

        if( (email == '') || (password== '') || (email == '' && password == '') ){
          this.setState({ visible2: true });   
        }
        
        else if(password.length < 6){
          this.setState({ visible3: true });
        }

        else if(password != password2){
          this.setState({ visible6: true });
        }

        else if( (email == '')||  (password== '')||  (check==false)){
             this.setState({ visible4: true });
        }

        else{

            try{
                firebase.auth().createUserWithEmailAndPassword(email, password);
               this.setState({ visible5: true });
              
            }
            catch(err){
                Alert.alert('Falha no cadastro.');
            }
        }
    }
        
 
  


  render() {
    const { navigate } = this.props.navigation;
    
    return (
   
    	<View style={styles.container}>
      
        <View style={styles.form}>

          <ImageBackground 
            source={require('./images/back.jpg')}
            style={{flex:1,alignItems:'center',}} 
          >


            <Modal animationType={'slide'} transparent = {true} visible = {this.state.visible1}>
            <ImageBackground 
    source={require('./images/back.jpg')}
    style={{flex:1,alignSelf:'center',
            alignItems:'center',
            justifyContent:'center'}} 
               >
              <View style={{
                flex:1,
                backgroundColor:'#transparent',
                alignSelf:'center',
                borderColor:'#000',
                borderWidth:3}}>

                <Text style={styles.textmodal}>1- ao instalar o aplicativo, o usúario declara e consente que o mesmo não equivale a um diagnóstico real feito
                                                por um médico.{"\n"}
                                               2- ao instalar o aplicativo, o usuário consente que a prioridade é sempre ir atrás de ajuda especializada{"\n"}
                                               3- ao instalar o aplicativo, o usuário consente que o aplicativo não pode ser submetido a nenhum tipo de processo pois é apenas um trabalho acadêmico.</Text>

                <TouchableOpacity style={styles.buttonmodal} onPress={this.handleCancel1}>
                  <Text style={styles.textbuttonmodal}> OK </Text>
                </TouchableOpacity>
              </View>
              </ImageBackground>
            </Modal>


            <Modal  animationType={'slide'} transparent = {true} visible = {this.state.visible2}>
            <ImageBackground 
    source={require('./images/back.jpg')}
    style={{width: 350,
          height: 200,
          marginTop:230,alignSelf:'center',
            alignItems:'center',
            justifyContent:'center'}} 
               >
             <View style={{
              width: 350,
              height: 200,
              backgroundColor:'#transparent',
              alignSelf:'center',
              borderColor:'#000',
              borderWidth:3}}>   

                <Text style={styles.textmodal}>Por favor, Insira um e-mail ou senha válido(s) para continuar.</Text>

                <TouchableOpacity style={styles.buttonmodal} onPress={this.handleCancel2}>
                  <Text style={styles.textbuttonmodal}> OK </Text>
                </TouchableOpacity>
              </View>
              </ImageBackground>
            </Modal>


      <Modal animationType={'slide'} transparent = {true} visible = {this.state.visible3}> 
      <ImageBackground 
    source={require('./images/back.jpg')}
    style={{width: 350,
          height: 200,
          marginTop:230,alignSelf:'center',
            alignItems:'center',
            justifyContent:'center'}} 
               >
      <View style={{
          width: 350,
          height: 200,
          backgroundColor:'#transparent',
          alignSelf:'center',
          borderColor:'#000',
          borderWidth:3}}>        
      <Text style={styles.textmodal}>Falha no cadastro, sua senha deve ter pelo menos 6 caracteres.</Text>
              <TouchableOpacity style={styles.buttonmodal2} onPress={this.handleCancel3}>
              <Text style={styles.textbuttonmodal}> OK </Text>
              </TouchableOpacity>
              </View>
        </ImageBackground>
      </Modal>


      <Modal  animationType={'slide'} transparent = {true} visible = {this.state.visible4}>
      <ImageBackground 
    source={require('./images/back.jpg')}
    style={{width: 350,
          height: 200,
          marginTop:230,alignSelf:'center',
            alignItems:'center',
            justifyContent:'center'}} 
               >  
      <View style={{
          width: 350,
          height: 200,
          backgroundColor:'#transparent',
          alignSelf:'center',
          borderColor:'#000',
          borderWidth:3}}>     
      <Text style={styles.textmodal}>Para cadastrar, vc precisa concordar com os termos de uso.</Text>
              <TouchableOpacity style={styles.buttonmodal2} onPress={this.handleCancel4}>
              <Text style={styles.textbuttonmodal}> OK </Text>
              </TouchableOpacity>
              </View>
            </ImageBackground>
      </Modal>

      <Modal  transparent = {true} visible = {this.state.visible5}>
      <ImageBackground 
    source={require('./images/back.jpg')}
    style={{width: 350,
          height: 200,
          marginTop:230,alignSelf:'center',
            alignItems:'center',
            justifyContent:'center'}} 
               >
      <View style={{
          width: 350,
          height: 200,
          backgroundColor:'#transparent',
          alignSelf:'center',
          borderColor:'#000',
          borderWidth:3}}>
              <Text style={styles.textmodal}>Cadastro concluido com sucesso.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.handleCancel5}>
              <Text style={styles.textbuttonmodal}> OK </Text>
              </TouchableOpacity>
              </View>
              </ImageBackground>
      </Modal>

       <Modal  transparent = {true} visible = {this.state.visible6}>
       <ImageBackground 
    source={require('./images/back.jpg')}
    style={{width: 350,
          height: 200,
          marginTop:230,alignSelf:'center',
            alignItems:'center',
            justifyContent:'center'}} 
               >
      <View style={{
          width: 350,
          height: 200,
          backgroundColor:'#transparent',
          alignSelf:'center',
          borderColor:'#000',
          borderWidth:3}}>
              <Text style={styles.textmodal}>As senhas digitadas não são iguais.</Text>
              <TouchableOpacity style={styles.buttonmodal} onPress={this.handleCancel6}>
              <Text style={styles.textbuttonmodal}> OK </Text>
              </TouchableOpacity>
              </View>
              </ImageBackground>
      </Modal>
				


         

    
					<Image style={styles.image} source={require('./images/registro.png')}/>

					
				


        <Text style = {styles.textLogin}> Para cadastrar é simples, basta inserir seu e-mail e uma senha de pelo menos 6 caracteres. </Text>
      
				<TextInput style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = "email@exemplo.com"
               placeholderTextColor = "#290039"
               autoCapitalize = "none"
               keyboardType='email-address'
               onChangeText={(email) => this.setState({email})} 
               value={this.state.email}
               />
            
            <TextInput style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = "senha123"
               placeholderTextColor = "#000"
               autoCapitalize = "none"
               onChangeText={(password) => this.setState({password})}
               value={this.state.password}
               Textinput secureTextEntry={true} 
               />

              <TextInput style = {styles.input}
               underlineColorAndroid = "transparent"
               placeholder = "senha123"
               placeholderTextColor = "#000"
               autoCapitalize = "none"
               onChangeText={(password2) => this.setState({password2})}
               value={this.state.password2}
               Textinput secureTextEntry={true} 
               />
             
             <Text  style={styles.textcheckbox} >*Declaro aceitar os </Text>
             <Text  style={styles.textcheckbox2} onPress={this.showModal1}> termos e condições de uso.</Text>

        <CheckBox value={this.state.check} 
        style={styles.checkbox}
        onValueChange={(check) => this.setState({check:true})}
        />
        

                 <TouchableOpacity style={styles.button} onPress={this.handleSignUp} >
          <Text style={styles.textButton} 
          > Juntar-se </Text>
        </TouchableOpacity>

                </ImageBackground>  
        
               </View>
             

			</View>
      
		);
	}
}

const styles = StyleSheet.create({
  container: {
  	backgroundColor: '#ACCEE8',
  	flex: 1,
    alignItems:'center'
  },
  button: {
    height: 40,
    width: 150,
    padding: 15,
    backgroundColor: '#FF7A7D',
    margin:15,
    borderColor: '#ff6505', 
    borderRadius:25,
    marginTop:17,
    borderColor:'#fff',
    borderWidth:2
  },
  button3:{
    height: 30,
    marginTop:-20,
    width: 120,
    padding: 15,
    backgroundColor: '#000',
    margin:15,
    right:100,
    borderColor: '#ff6505',
  },
  textmodal: {
    color: '#000',
    fontWeight: '700',
    fontSize: 25
  },
  textbuttonmodal: {
    color: '#000',
    fontWeight: '700',
    fontSize: 20,
    marginTop:-13,
    left:17
  },
  buttonmodal:{
    height: 35,
    width: 110,
    padding: 15,
    backgroundColor: '#FF7A7D',
    margin:15,
    borderColor: '#FF7A7D',
    alignSelf:'flex-end',
    marginTop:65,
    borderColor:'#fff',
    borderWidth:2,
  },
  buttonmodal2:{
    height: 35,
    width: 110,
    padding: 15,
    backgroundColor: '#FF7A7D',
    margin:15,
    borderColor: '#FF7A7D',
    alignSelf:'flex-end',
    marginTop:40,
    borderColor:'#fff',
    borderWidth:2,
  },

  textcheckbox: {
    fontSize:12,
    marginTop:-14,
    right:83,
    color:'#000',
    fontWeight:'400'
  },
  textcheckbox2: {
    fontSize:12,
    marginTop:-15,
    left:43,
    top:-1,
    color:'#000',
    fontWeight:'700',
    textDecorationLine:'underline'
  },
  checkbox: {
    marginTop:-25,
    left:130
  },
  textButton: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 20,
    marginTop:-10
  },
  textButton2: {
    color: '#290039',
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 15,
    marginTop:-10
  },
  textButton3:{
    color: '#fff',
    textAlign: 'center',
    fontWeight: '500',
    fontSize: 10,
    marginTop:-14,
    margin:0
  },

  input: {
    height: 55,
    width: 325,
    backgroundColor: '#FF7A7D',
    padding: 10,
    color: '#000',
    margin:30,
    marginTop:-5,
    alignItems:'center',
    justifyContent:'center',
    fontSize:20,
    borderRadius:25,
   },
textLogin: {
    height: 100,
    margin:5,
    padding:10,
    fontSize: 20,
    marginTop:-100,
    alignItems:'center',
    justifyContent:'center',
    color: '#000',
    textAlign: 'center',
    fontWeight: '900',
    fontSize: 19
},
  containersub: {
    alignItems: 'center',
    marginTop:50,
    justifyContent: 'center', 
    },
  inner: {
	  flexDirection: 'column',
	  justifyContent: 'center', 
	  alignItems: 'center'
  },
  image: { 
    margin:0,
  	width:  270, 
  	height: 270,
    marginTop:0
  	  },
  form: {
  	justifyContent: 'center', 
	  alignItems: 'center',
  }
   
 });

export default Page2;
