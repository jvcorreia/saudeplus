import React, { Component } from 'react';
import { Text, View, Image, TextInput, TouchableOpacity, StyleSheet ,Alert, ScrollView,
 ImageBackground, Modal} from 'react-native';


 class Page9 extends Component {
   static navigationOptions = {
    header: null,
  };



  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
       <ImageBackground 
          source={require('./images/back.jpg')}
          style={{flex:1}} 
        >

     
      <View style={styles.inner}>
        <Image style={styles.image} source={require('./images/mapadasaude.png')}/>
             
        <TouchableOpacity style={styles.button} onPress={() => this.props.navigation.navigate('FarmaciaMaps')} >
          <Image style={styles.image2} source={require('./images/farmacia.png')}/>
        </TouchableOpacity>

        <Text style={styles.text1}>Farmácias próximas </Text>

        <TouchableOpacity style={styles.button5} onPress={() => this.props.navigation.navigate('HospitalMaps')}>
          <Image style={styles.image2} source={require('./images/hospital.png')}/>
        </TouchableOpacity>

        <Text style={styles.text11}>Hospitais próximos</Text>

        <TouchableOpacity style={styles.buttonsegundo} onPress={() => this.props.navigation.navigate('PostoDeSaudeMaps')} >
          <Image style={styles.image2} source={require('./images/agulha.png')}/>
        </TouchableOpacity>

        <Text style={styles.text2}>Postos de saúde{"\n"} próximos</Text>
        
        <TouchableOpacity style={styles.buttonterceiro} onPress={() => this.props.navigation.navigate('ClinicaPsicologiaMaps')}>
          <Image style={styles.image2} source={require('./images/clinicapsicologia.png')}/>
        </TouchableOpacity>

        <Text style={styles.text3}>Clínicas psicológicas{"\n"} próximas</Text>

        <TouchableOpacity style={styles.button2} onPress={() => this.props.navigation.navigate('Menu')} >
          <Text style={styles.textButton2}> Voltar </Text>
        </TouchableOpacity>

        
        
      </View>
          </ImageBackground>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ACCEE8',
    flex: 1
  },
  inner: {
    justifyContent: 'flex-start' 
  },
  image: {  
    width:  270, 
    height: 270,
    alignItems:'flex-start',
    left:47,
    top:-30,
    marginTop:-35
  },
  image2: {
    width:  50, 
    height: 50,
    top:7,
    left:9,
    alignItems:'center',
    justifyContent:'center'
  },
  button: {
    height: 100,
    width: 100,
    padding: 15,
    backgroundColor: '#FF7A7D',
    margin:15,
    top:-103,
    left:20,
    borderColor: '#ff6505', 
    borderRadius:50,
    marginTop:45,
    borderColor:'#fff',
    borderWidth:2
  },
  button5: {
    height: 100,
    width: 100,
    padding: 15,
    backgroundColor: '#FF7A7D',
    margin:15,
    top:-80,
    left:20,
    borderColor: '#ff6505', 
    borderRadius:50,
    marginTop:45,
    borderColor:'#fff',
    borderWidth:2
  },
  buttonsegundo: {
    height: 100,
    width: 100,
    padding: 15,
    backgroundColor: '#FF7A7D',
    margin:15,
    top:-429,
    left:200,
    borderColor: '#ff6505', 
    borderRadius:50,
    marginTop:45,
    borderColor:'#fff',
    borderWidth:2
  },
  buttonterceiro: {
    height: 100,
    width: 100,
    padding: 15,
    backgroundColor: '#FF7A7D',
    margin:15,
    top:-446,
    left:200,
    borderColor: '#ff6505', 
    borderRadius:50,
    marginTop:45,
    borderColor:'#fff',
    borderWidth:2
  },
   button2: {
    height: 37,
    width: 140,
    padding: 15,
    marginTop:-30,
    left:25,
    backgroundColor: '#FF7A7D',
    top:-370,
    borderColor: '#ff6505', 
    borderRadius:25,
    borderColor:'#fff',
    borderWidth:2
  },
  text: {
    color: '#000',
    fontWeight: '700',
    fontSize: 14,
    left:250,
    top:-59,
    textDecorationLine:'underline'
  },
  text1: {
    color: '#000',
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 17,
    top:-87,
    marginTop:-20,
    right:87
  },
  text11: {
    color: '#000',
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 17,
    top:-67,
    marginTop:-20,
    right:87
  },
  text2: {
    color: '#000',
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 17,
    top:-433,
    left:87
  },
  text3: {
    color: '#000',
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 17,
    top:-450,
    left:87
  },
  textButton2: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 20,
    marginTop:-12
  },
  textButton3: {
    color: '#000',
    textAlign: 'center',
    fontWeight: '200',
    fontSize: 15,
    top:-370,
    left:87,
    textDecorationLine:'underline'
  },
  textlogin: {
    height: 100,
    margin:5,
    padding:10,
    fontSize: 20,
    alignItems:'center',
    justifyContent:'center',
    color: '#000',
    textAlign: 'center',
    fontWeight: '900',
    fontSize: 19
},
  textmodal: {
    margin:5,
    padding:10,
    fontSize: 28,
    alignItems:'center',
    justifyContent:'center',
    color: '#000',
    textAlign: 'center',
    fontWeight: '900'
},
  textmodal2: {
    margin:5,
    padding:10,
    fontSize: 19,
    alignItems:'center',
    justifyContent:'center',
    color: '#000',
    textAlign: 'center',
    fontWeight: '600',
    margin:10
},
  buttonmodal: {
    height: 37,
    width: 140,
    padding: 15,  
    marginTop:10, 
    backgroundColor: '#FF7A7D',
    borderColor: '#ff6505', 
    borderRadius:25,
    borderColor:'#fff',
    borderWidth:2,
    alignSelf:'center'
  },
  text4: {
    color: '#000',
    textAlign: 'center',
    fontWeight: '100',
    fontSize: 20,
    marginTop:30,
    textDecorationLine:'underline'
  },
  textbuttonmodal: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 18,
    marginTop:-12
  }

});


 export default Page9;