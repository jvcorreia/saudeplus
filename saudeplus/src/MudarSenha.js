import React, { Component } from 'react';
import { Text, View, Image, TextInput, TouchableOpacity, StyleSheet
 ,Alert, Modal, ScrollView, ImageBackground} from 'react-native';
import firebase from 'react-native-firebase';


 class Page7 extends Component {
   static navigationOptions = {
    header: null,
  };

state = { email: ''}



 tentarsenha = async() => {

        const { email} = this.state;
        firebase.auth().sendPasswordResetEmail(email)
      .then(function (user) {
        alert('E-mail enviado. Por favor, cheque o seu e-mail.')
      }).catch(function (e) {
        console.log(e)
      })
     
    }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}> 
      <ImageBackground 
    source={require('./images/back.jpg')}
    style={{flex:1,alignItems:'center',}} 
>
      <View style={styles.image}> 

      <Image style={styles.image} source={require('./images/mudarsenha.png')}/> 
      </View>     
               <Text style={styles.textlogin}> Olá, digite abaixo o e-mail cadastrado. A Saúde+ enviará um e-mail com
               intruções para mudança de senha. </Text>
                <TextInput style = {styles.input2}

               underlineColorAndroid = "transparent"
               placeholder = "E-mail:"
               placeholderTextColor = "#000"
               autoCapitalize = "none"
               keyboardType='default'
               onChangeText={(email) => this.setState({email})}
               value={this.state.email}
               />  
                <TouchableOpacity style={styles.button} 
               
              onPress={this.tentarsenha} >
          <Text style={styles.textButton} > Enviar e-mail  </Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.button2} 
               
               >
          <Text style={styles.textButton} onPress={() => this.props.navigation.navigate('Login')}> Voltar  </Text>
        </TouchableOpacity>
</ImageBackground>
        </View>



    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ACCEE8',
    flex: 1,
    justifyContent:'center',
    alignItems:'center'
  },
 containermodal: {
    backgroundColor: '#FFDBD9',
    justifyContent:'center',
    alignItems:'center'
  },

  inner: {
  justifyContent: 'center', 
  alignItems: 'center',
  
  },

  image: { 
    width:  300, 
    height: 300,
    
      },

input: {
     height: 50,
    width: 300,
    backgroundColor: '#FF7A7D',
    padding: 10,
    color: '#000',
    margin:15,
    marginTop:-30,
    borderRadius:20,
    fontSize:20,

   },
   input2: {
     height: 50,
    width: 300,
    backgroundColor: '#FF7A7D',
    padding: 10,
    color: '#000',
    margin:15,
    marginTop:0,
    borderRadius:20,
    fontSize:20,
    
   },
   

button: {
    height: 45,
    width: 160,
    padding: 15,
    backgroundColor: '#FF7A7D',
    margin:10,
    borderColor: '#ff6505', 
    borderRadius:25,
    left:83,
      borderColor:'#fff',
    borderWidth:2
    
    

  },

  button2: {
   
    height: 45,
    width: 160,
    padding: 15,
    backgroundColor: '#FF7A7D',
    margin:10,
    borderColor: '#ff6505', 
    borderRadius:25,
    top:-63,
    right:83,
    borderColor:'#fff',
    borderWidth:2

  },
       buttonmodal:{
     height: 30,
    width: 100,
    padding: 15,
    backgroundColor: '#FF7A7D',
    margin:15,
    borderColor: '#FF7A7D',
    alignSelf:'flex-end',
    marginTop:95
    

  },
       buttonmodal2:{
     height: 30,
    width: 100,
    padding: 15,
    backgroundColor: '#FF7A7D',
    margin:15,
    borderColor: '#FF7A7D',
    alignSelf:'flex-end',
    marginTop:70
    

  },
   textmodal: {
    color: '#000',
    fontWeight: '700',
    fontSize: 25
    

  },
    textbuttonmodal: {
    color: '#000',
    fontWeight: '700',
    fontSize: 20,
    marginTop:-13,
    left:17

    

  },
    textesqueceusenha: {
      fontSize: 14,      
      color: '#000',
    fontWeight: '800',
    right: 80,
    textDecorationLine:'underline',
    top:14
},
  textButton: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 19,
    marginTop:-7,

  },
  textlogin: {
      height: 120,
      margin:5,
      padding:10,
      fontSize: 20,
      
      
      alignItems:'center',
      justifyContent:'center',
      color: '#000',
    textAlign: 'center',
    fontWeight: '900',
    fontSize: 19,

}

});

 export default Page7;