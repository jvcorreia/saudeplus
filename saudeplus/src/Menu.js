import React, { Component } from 'react';
import { Text, View, Image, TextInput, TouchableOpacity, StyleSheet ,Alert, ScrollView,
 ImageBackground, Modal} from 'react-native';
import firebase from 'react-native-firebase';
import {
  LoginButton,
  AccessToken
} from 'react-native-fbsdk';
import { LoginManager } from 'react-native-fbsdk';
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';


 class Page4 extends Component {
   static navigationOptions = {
    header: null,
  };


state = {visible1:false, visible2:false, visible3:false, visible4:false}

  showModal1 = () => {
    this.setState({ visible1: true });
    this.setState({ visible4: false });
  }
  showModal2 = () => {
    this.setState({ visible2: true });
    this.setState({ visible4: false });
  }
  showModal3 = () => {
    this.setState({ visible3: true });
    this.setState({ visible4: false });
  }
  showModal4 = () => {
    this.setState({ visible4: true });
  }
  handleCancel1 = () => {
    this.setState({ visible1: false });
  }
  handleCancel2 = () => {
    this.setState({ visible2: false });
  }
 handleCancel3 = () => {
    this.setState({ visible3: false });
  }
  handleCancel4 = () => {
    this.setState({ visible4: false });
  }


  signOutUser ()  {
    firebase.auth().signOut();
    LoginManager.logOut();
    this.props.navigation.navigate('Index');
 };


  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <ImageBackground 
          source={require('./images/back.jpg')}
          style={{flex:1}} 
        >


          <Modal animationType={'slide'} transparent = {true} visible = {this.state.visible4}>
            <ImageBackground 
              source={require('./images/back.jpg')}
              style={{flex:0.4,}} 
            >
              <View style={{
                flex:1,
                backgroundColor:'#transparent',
                borderColor:'#000',
                borderWidth:3
              }}>
              
                <Text style={styles.text4} onPress={this.showModal1}> Diagnóstico Saúde+ </Text>
                <Text style={styles.text4} onPress={this.showModal2}> Mapa da saúde </Text>            
                <Text style={styles.text4} onPress={this.showModal3}> Recomendações </Text>
              
                <TouchableOpacity style={styles.buttonmodal} onPress={this.handleCancel4}>
                  <Text style={styles.textbuttonmodal}> Voltar </Text>
                </TouchableOpacity>
              
              </View>
            </ImageBackground>
          </Modal>

          <Modal animationType={'slide'} transparent = {true} visible = {this.state.visible1}>
            <ImageBackground 
              source={require('./images/back.jpg')}
              style={{flex:0.7,alignSelf:'center',
              alignItems:'center',
              justifyContent:'center'}} 
            >
              <View style={{
                flex:1,
                backgroundColor:'#transparent',
                borderColor:'#000',
                borderWidth:3,alignSelf:'center',
                alignItems:'center',
                justifyContent:'center'
              }}>
                <Text style={styles.textmodal}>Diagnóstico Saúde+</Text>
                <Text style={styles.textmodal2}>O diagnóstico é muito simples: são 50 sintomas para você marcar, e em cada linha estão em média 8 sintomas. Basta puxar para o
                  lado e ir marcando para nos dizer o que está sentindo. Pode ser que o diagnóstico acuse mais de um problema ou não acuse nenhum, nestes casos iremos recomendar uma ida ao hospital mais próximo.</Text>
              
                <TouchableOpacity style={styles.buttonmodal} onPress={this.handleCancel1}>
                  <Text style={styles.textbuttonmodal}> OK </Text>
                </TouchableOpacity>
              
              </View>
            </ImageBackground>
          </Modal>

          <Modal animationType={'slide'} transparent = {true} visible = {this.state.visible2}>
            <ImageBackground 
              source={require('./images/back.jpg')}
              style={{flex:0.6,alignSelf:'center',
              alignItems:'center',
              justifyContent:'center'}} 
            >
              <View style={{
                flex:1,
                backgroundColor:'transparent',
                borderColor:'#000',
                borderWidth:3
              }}>
                <Text style={styles.textmodal}>Mapa da saúde</Text>
                <Text style={styles.textmodal2}>Por aqui você poderá encontar, a partir da sua localização,
                  as farmácias, hospitais, postos de saúde e clínicas psicológicas mais próximas na sua região, basta dar um clique e pronto.
                  O login com a google pode ajudar a melhorar a precisão.</Text>
              
                <TouchableOpacity style={styles.buttonmodal} onPress={this.handleCancel2}>
                  <Text style={styles.textbuttonmodal}> OK </Text>
                </TouchableOpacity>
              </View>
            </ImageBackground>
          </Modal>

          <Modal animationType={'slide'} transparent = {true} visible = {this.state.visible3}>
            <ImageBackground 
              source={require('./images/back.jpg')}
              style={{flex:0.5,alignSelf:'center',
              alignItems:'center',
              justifyContent:'center'}} 
            >
              <View style={{
                flex:1,
                backgroundColor:'#transparent',
                borderColor:'#000',
                borderWidth:3}}>
                <Text style={styles.textmodal}>Recomendações</Text>
                <Text style={styles.textmodal2}>Aqui você poderá encontrar indicações para 15 doenças, como o que fazer, tratamentos e remédios mais
                  indicados com seus preços médios.</Text>
              
                <TouchableOpacity style={styles.buttonmodal} onPress={this.handleCancel3}>
                  <Text style={styles.textbuttonmodal}> OK </Text>
                </TouchableOpacity>
              </View>
            </ImageBackground>
          </Modal>

     
      <View style={styles.inner}>
        <Image style={styles.image} source={require('./images/menu.png')}/>
             
        <TouchableOpacity style={styles.button} onPress={() => this.props.navigation.navigate('Diagnostico')} >
          <Image style={styles.image2} source={require('./images/diagicon.png')}/>
        </TouchableOpacity>

        <Text style={styles.text1}>Diagnóstico+ </Text>

        <TouchableOpacity style={styles.button} onPress={() => this.props.navigation.navigate('MapaDaSaude')}>
          <Image style={styles.image2} source={require('./images/locationicon.png')}/>
        </TouchableOpacity>

        <Text style={styles.text1}>Mapa da saúde</Text>

        <TouchableOpacity style={styles.buttonsegundo} onPress={() => this.props.navigation.navigate('Recomendacao')} >
          <Image style={styles.image2} source={require('./images/recomendicon.png')}/>
        </TouchableOpacity>

        <Text style={styles.text2}>Recomendações </Text>
        
        <TouchableOpacity style={styles.buttonterceiro} onPress={() => this.props.navigation.navigate('Historia')}>
          <Image style={styles.image2} source={require('./images/contacticon.png')}/>
        </TouchableOpacity>

        <Text style={styles.text3}>História e contatos</Text>

        <TouchableOpacity style={styles.button2} onPress={() => {this.signOutUser()}} >
          <Text style={styles.textButton2}> Sair </Text>
        </TouchableOpacity>

        <Text style={styles.textButton3} onPress = {this.showModal4}> Preciso de ajuda </Text>
        
      </View>
        </ImageBackground>     
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ACCEE8',
    flex: 1
  },
  inner: {
    justifyContent: 'flex-start' 
  },
  image: {  
    width:  220, 
    height: 220,
    alignItems:'flex-start',
    left:80,
    top:-30
  },
  image2: {
    width:  50, 
    height: 50,
    top:7,
    left:9,
    alignItems:'center',
    justifyContent:'center'
  },
  button: {
    height: 100,
    width: 100,
    padding: 15,
    backgroundColor: '#FF7A7D',
    margin:15,
    top:-84,
    left:20,
    borderColor: '#ff6505', 
    borderRadius:50,
    marginTop:45,
    borderColor:'#fff',
    borderWidth:2
  },
  buttonsegundo: {
    height: 100,
    width: 100,
    padding: 15,
    backgroundColor: '#FF7A7D',
    margin:15,
    top:-414,
    left:200,
    borderColor: '#ff6505', 
    borderRadius:50,
    marginTop:45,
    borderColor:'#fff',
    borderWidth:2
  },
  buttonterceiro: {
    height: 100,
    width: 100,
    padding: 15,
    backgroundColor: '#FF7A7D',
    margin:15,
    top:-431,
    left:200,
    borderColor: '#ff6505', 
    borderRadius:50,
    marginTop:45,
    borderColor:'#fff',
    borderWidth:2
  },
   button2: {
    height: 37,
    width: 140,
    padding: 15,
    marginTop:-30,
    left:25,
    backgroundColor: '#FF7A7D',
    top:-340,
    borderColor: '#ff6505', 
    borderRadius:25,
    borderColor:'#fff',
    borderWidth:2
  },
  text: {
    color: '#000',
    fontWeight: '700',
    fontSize: 14,
    left:250,
    top:-44,
    textDecorationLine:'underline'
  },
  text1: {
    color: '#000',
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 18,
    top:-68,
    marginTop:-20,
    right:87
  },
  text2: {
    color: '#000',
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 18,
    top:-418,
    left:87
  },
  text3: {
    color: '#000',
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 18,
    top:-435,
    left:87
  },
  textButton2: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 20,
    marginTop:-12
  },
  textButton3: {
    color: '#000',
    textAlign: 'center',
    fontWeight: '200',
    fontSize: 15,
    top:-370,
    left:87,
    textDecorationLine:'underline'
  },
  textlogin: {
    height: 100,
    margin:5,
    padding:10,
    fontSize: 20,
    alignItems:'center',
    justifyContent:'center',
    color: '#000',
    textAlign: 'center',
    fontWeight: '900',
    fontSize: 19
},
  textmodal: {
    margin:5,
    padding:10,
    fontSize: 28,
    alignItems:'center',
    justifyContent:'center',
    color: '#000',
    textAlign: 'center',
    fontWeight: '900'
},
  textmodal2: {
    margin:5,
    padding:10,
    fontSize: 19,
    alignItems:'center',
    justifyContent:'center',
    color: '#000',
    textAlign: 'center',
    fontWeight: '600',
    margin:10
},
  buttonmodal: {
    height: 37,
    width: 140,
    padding: 15,  
    marginTop:10, 
    backgroundColor: '#FF7A7D',
    borderColor: '#ff6505', 
    borderRadius:25,
    borderColor:'#fff',
    borderWidth:2,
    alignSelf:'center'
  },
  text4: {
    color: '#000',
    textAlign: 'center',
    fontWeight: '100',
    fontSize: 20,
    marginTop:30,
    textDecorationLine:'underline'
  },
  textbuttonmodal: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 18,
    marginTop:-12
  }

});

 export default Page4;