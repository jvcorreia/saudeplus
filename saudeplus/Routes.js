import React, { Component } from "react";

import { createStackNavigator, createAppContainer } from 'react-navigation';

import Page4 from './src/Menu';
import Page1 from './src/Index';
import Page2 from './src/Cadastro';
import Page3 from './src/FarmaciaMaps';
import Page11 from './src/Login';
import Page5 from './src/Diagnostico';
import Page6 from './src/Recomendacao';
import Page7 from './src/MudarSenha';
import Page8 from './src/Historia';
import Page9 from './src/MapaDaSaude';
import Page10 from './src/HospitalMaps';
import Page12 from './src/PostoDeSaudeMaps';
import Page13 from './src/ClinicaPsicologiaMaps';
import Page14 from './src/ChatSonho';
import Page15 from './src/LinkedIn';






const Project= createStackNavigator({
    Index: {screen: Page1},
    Cadastro: {screen: Page2},
    FarmaciaMaps: {screen: Page3},
    Menu: {screen: Page4},
    Diagnostico: {screen: Page5},
    Recomendacao: {screen: Page6},
    MudarSenha: {screen: Page7},
    Historia: {screen: Page8},
    MapaDaSaude: {screen: Page9},
    HospitalMaps: {screen: Page10},
    Login: {screen: Page11},
    PostoDeSaudeMaps: {screen: Page12},
    ClinicaPsicologiaMaps: {screen: Page13},
    ChatSonho: {screen: Page14},
    LinkedIn: {screen: Page15}
    
});

export default createAppContainer(Project);
