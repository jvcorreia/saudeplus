/**
 * @format
 */
import { AppRegistry } from "react-native";
import { createAppContainer } from "react-navigation";
import Navigator from "./Navigator";
import { name as appName } from "./app.json";
console.disableYellowBox = true;
AppRegistry.registerComponent(appName, () => Navigator);
